BASIC USAGE
---------------

1. Enable this module, as usual.

2. Go to /admin/structure/auctioneer. All settings are grouped here.

3. Go to /admin/structure/auctioneer/settings/bid_type and add a new Bid type.

4. Go to /admin/structure/auctioneer/settings/auction_type and add a new
   Auction type. Pick the bid type you created at previous step.

5. Now, go to /admin/structure/auctioneer/auctions and add a new auction of
   the type you've just created.


AVAILABLE DRUSH COMMANDS
---------------

1. drush auctioneer:purge_orphan_bids
   Purge/remove bids which parent auction has been deleted. It is also possible
   to use:
   drush auctioneer-pob --limit=30
   Previous command will process an amount of 30 bids max.

2. drush auctioneer:place_auction_statistics AUCTION_ID
   Initialize or update statistics for an existing auction. Also, you can to:
   drush auctioneer-pas AUCTION_ID

3. drush auctioneer:display_auction_statistics AUCTION_ID
   Display statistics for a specified auction. Currently, it's possible to
   display text plain data or JSON format, by using:
   drush auctioneer-das AUCTION_ID --format=json

4. drush auctioneer:check_auction_dates
   Check/close auctions where its date range has ended. It is also possible to:
   drush auctioneer-cad --limit=30
   Previous command will process an amount of 30 auctions max.
