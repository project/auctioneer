<?php

/**
 * @file
 * Post update functions for Auctioneer.
 */

/**
 * Reset auctions data in order to match with new added fields.
 */
function auctioneer_post_update_reset_auctions_data(&$sandbox) {
  $auction_type_manager = \Drupal::service('auctioneer.auction_type.manager');
  $entity_type_manager = \Drupal::service('entity_type.manager');
  if (!isset($sandbox['total'])) {
    $available = $entity_type_manager->getStorage('auction')->getQuery()->execute();
    $sandbox['total'] = count($available);
    $sandbox['available'] = array_values($available);
    $sandbox['current'] = 0;
    $sandbox['items_per_batch'] = 30;
  }
  $available = array_slice(
    $sandbox['available'],
    $sandbox['current'],
    $sandbox['current'] + $sandbox['items_per_batch']
  );
  $auctions = $entity_type_manager->getStorage('auction')->loadMultiple($available);
  foreach ($auctions as $auction) {
    if ($hammer = $auction_type_manager->getAuctionHammerBid($auction)) {
      // This method also calls the statistics update.
      $auction_type_manager->setAuctionHammerBid($hammer);
    }
    else {
      // If no hammer, then we need to directly reset statistics.
      $auction_type_manager->setAuctionStatistics($auction);
    }
    $sandbox['current']++;
  }
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  }
  else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }

  return t('@percentage% of auctions updated (@current of @total).', [
    '@percentage' => $sandbox['total'] > 0 ? round($sandbox['current'] / ($sandbox['total'] * 100)) : 0,
    '@current' => $sandbox['current'],
    '@total' => $sandbox['total'],
  ]);
}
