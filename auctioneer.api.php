<?php

/**
 * @file
 * Hooks provided by the auctioneer module.
 */

use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\AuctioneerContentEntityInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter auction contexts.
 */
function hook_auctioneer_auction_context_alter(AuctionInterface $auction, array $plugin_definition, array &$context) {
  array_push($context, 'custom_context');
}

/**
 * Provide custom handler events.
 */
function hook_auctioneer_handler_event_info() {
  return [
    'module_name_event_key' => [
      'label' => t('My custom event'),
      'description' => t('This is a custom event implementation.'),
      'scope' => ['bid', 'auction'],
    ],
  ];
}

/**
 * Alter auctioneer shortcuts for bids and auctions.
 */
function hook_auctioneer_shortcuts_alter(array &$shortcuts, AuctioneerContentEntityInterface $entity, array $context) {
  $shortcuts['my_shortcut'] = [
    '#type' => 'html_tag',
    '#tag' => 'span',
    '#value' => $entity->id(),
  ];
}

/**
 * @} End of "addtogroup hooks".
 */
