<?php

namespace Drupal\auctioneer_datetime_range;

use Drupal\auctioneer\AuctionTypeManagerInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;

/**
 * Interface DateRangeAuctionTypeManagerInterface.
 */
interface DatetimeRangeAuctionTypeManagerInterface {

  /**
   * Returns the auction type manager.
   */
  public function auctionTypeManager() : AuctionTypeManagerInterface;

  /**
   * Get a list of N items of date ranged auctions.
   *
   * @param array $params
   *   Some parameters for this query.
   */
  public function getDatetimeRangeAuctionTypeAuctions(array $params = []) : array;

  /**
   * Check a list of N items of date ranged auctions.
   *
   * @param array $params
   *   Some parameters for this checking.
   */
  public function checkDatetimeRangeAuctionTypeAuctions(array $params = []) : array;

  /**
   * Check if provided auction type is date-ranged instance.
   *
   * @param \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type
   *   The auction type entity object.
   *
   * @return bool
   *   TRUE or FALSE depending if it is date-ranged instance.
   */
  public function auctionTypeIsDatetimeRangeInstance(AuctionTypeInterface $auction_type) : bool;

}
