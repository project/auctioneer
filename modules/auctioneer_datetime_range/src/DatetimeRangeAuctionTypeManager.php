<?php

namespace Drupal\auctioneer_datetime_range;

use Drupal\auctioneer\AuctionTypeManagerInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;

/**
 * Provides checker for auction types.
 */
class DatetimeRangeAuctionTypeManager implements DatetimeRangeAuctionTypeManagerInterface {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager) {
    $this->auctionTypeManager = $auction_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function auctionTypeManager() : AuctionTypeManagerInterface {
    return $this->auctionTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatetimeRangeAuctionTypeAuctions(array $params = []) : array {
    $limit = $params['limit'] ?? 30;
    $offset = $params['offset'] ?? 0;
    $context = DatetimeRangeAuctionTypeInterface::AUCTION_TYPE_CONTEXT_KEY;
    $result = $this->auctionTypeManager->database()
      ->query("
        SELECT a.id FROM {auction} a
        INNER JOIN {auctioneer_auction_statistics} s ON a.id = s.auction_id
        INNER JOIN {auction_field_data} d ON a.id = d.id
        WHERE s.context LIKE '%$context%'
        AND d.auction_state != 'closed'
        ORDER BY a.id ASC
        LIMIT $offset, $limit
      ")
      ->fetchCol();
    $auctions = !empty($result) ? $this->auctionTypeManager->entityTypeManager()->getStorage('auction')->loadMultiple($result) : [];

    return $auctions;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDatetimeRangeAuctionTypeAuctions(array $params = []) : array {
    $data = [
      '_auctions' => $this->getDatetimeRangeAuctionTypeAuctions($params),
      'processed' => [],
      'acquitted' => [],
    ];
    $plugin_manager = $this->auctionTypeManager->auctionPluginManager();
    foreach ($data['_auctions'] as $auction) {
      $auction_plugin_id = $auction->getAuctionType()->getBidType()->getPluginName();
      $plugin_instance = $plugin_manager->createInstance($auction_plugin_id);
      // Now, calling the plugin date checking.
      if ($plugin_instance->auctionIsInDateRange($auction)) {
        array_push($data['acquitted'], $auction);
      }
      else {
        $auction->set('auction_state', 'closed');
        $auction->save();
        array_push($data['processed'], $auction);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function auctionTypeIsDatetimeRangeInstance(AuctionTypeInterface $auction_type) : bool {
    $bid_type = $auction_type->getBidType();
    $plugin_definition = $bid_type ? $this->auctionTypeManager->getBidTypePluginDefinition($bid_type) : [];
    if (isset($plugin_definition['class'])) {
      $reflection = new \ReflectionClass($plugin_definition['class']);

      return $reflection->implementsInterface('\Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeInterface');
    }

    return FALSE;
  }

}
