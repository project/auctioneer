<?php

namespace Drupal\auctioneer_datetime_range\Plugin\Auctioneer\AuctionType;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the "Default - Descending" auction type.
 *
 * @AuctionType(
 *   id = "auctioneer_auction_type_default_descending",
 *   label = "Default - Descending"
 * )
 */
class DefaultDescending extends AuctionTypeDefault {

  /**
   * {@inheritdoc}
   */
  public function processBidForm(array &$form, FormStateInterface $form_state) {
    parent::processBidForm($form, $form_state);
    if (!isset($form['#access']) || $form['#access']) {
      /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
      $bid = $form_state->getFormObject()->getEntity();
      if ($bid) {
        /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
        $auction = $bid->getAuction();
        if ($auction) {
          $next_amount = $this->getNetxRequiredBidAmountByAuction($auction, 'descending');
          // Setting next bid amount as form attribute to improve usability.
          $form['#attributes']['data-next-bid'] = $next_amount;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateBidForm(array &$form, FormStateInterface $form_state) {
    parent::validateBidForm($form, $form_state);
    $form_values = $form_state->getValues();
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
    $auction = $bid->getAuction();
    if ($auction) {
      $control_bid_amount = reset($form_values['bid_amount'])['number'];
      $required_bid_amount = $this->getNetxRequiredBidAmountByAuction($auction, 'descending');
      if ($this->compareAmounts($control_bid_amount, $required_bid_amount) === 1) {
        $form_state->setErrorByName(
          'bid_amount',
          $this->t('Bid amount should be lower than or equal to $@required_bid_amount.', [
            '@required_bid_amount' => number_format($required_bid_amount, 2, ',', ' '),
          ])
        );
      }
    }
  }

}
