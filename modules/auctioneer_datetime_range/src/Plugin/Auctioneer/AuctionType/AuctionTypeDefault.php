<?php

namespace Drupal\auctioneer_datetime_range\Plugin\Auctioneer\AuctionType;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\auctioneer\AuctionType;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeInterface;

/**
 * Base class for default auction types.
 */
abstract class AuctionTypeDefault extends AuctionType implements DatetimeRangeAuctionTypeInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getAuctionBaseFieldDefinitions() {
    $fields = [
      'auction_dates' => BaseFieldDefinition::create('daterange')
        ->setLabel($this->t('Auction availability'))
        ->setRequired(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'daterange_default',
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
      'auction_initial_amount' => BaseFieldDefinition::create('auctioneer_number')
        ->setLabel($this->t('Initial amount'))
        ->setSettings([
          'max_length' => 19,
          'allow_negative' => TRUE,
          'allow_decimal' => TRUE,
        ])
        ->setDisplayOptions('form', [
          'type' => 'auctioneer_number_default',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setRequired(TRUE),
      'auction_bid_step_value' => BaseFieldDefinition::create('auctioneer_number')
        ->setLabel($this->t('Bid step'))
        ->setSettings([
          'max_length' => 19,
          'allow_negative' => FALSE,
          'allow_decimal' => TRUE,
        ])
        ->setDisplayOptions('form', [
          'type' => 'auctioneer_number_default',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setRequired(TRUE),
      'auction_bid_step_type' => BaseFieldDefinition::create('list_string')
        ->setSettings([
          'allowed_values_function' => '\Drupal\auctioneer_datetime_range\Plugin\Auctioneer\AuctionType\AuctionTypeDefault::getAllowedBidStepTypes',
        ])
        ->setLabel('Step type')
        ->setRequired(TRUE)
        ->setCardinality(1)
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getBidBaseFieldDefinitions() {
    $fields = [
      'bid_amount' => BaseFieldDefinition::create('auctioneer_number')
        ->setLabel($this->t('Amount'))
        ->setSettings([
          'max_length' => 19,
          'allow_negative' => FALSE,
          'allow_decimal' => TRUE,
        ])
        ->setDisplayOptions('view', [
          'label' => 'hidden',
          'type' => 'auctioneer_number_default',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setRequired(TRUE),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function processBidForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
    $auction = $bid->getAuction();
    if (!$this->auctionIsInDateRange($auction) && !in_array($auction->getAuctionState(), [
      'paused',
      'closed',
    ])) {
      // Don't show form if current date is not into the auction defined range.
      $form['#access'] = FALSE;
    }
    $form['#attached']['library'][] = 'auctioneer_datetime_range/bid.form.default_behavior';
  }

  /**
   * {@inheritdoc}
   */
  public function validateBidForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
    $auction = $bid->getAuction();
    if ($auction) {
      // Just a last stand.
      if (in_array($auction->getAuctionState(), ['paused', 'closed'])) {
        $form_state->setError($form, $this->t('Auction "@auction_label" is not accepting bids.', ['@auction_label' => $auction->label()]));
      }
      if (!$this->auctionIsInDateRange($auction)) {
        $form_state->setError($form, $this->t('Auction "@auction_label" is not into a valid date range.', ['@auction_label' => $auction->label()]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function auctionIsInDateRange(AuctionInterface $auction) : bool {
    $auction_dates = $auction->get('auction_dates')->first();
    // Needed to compare current date with provided ones in auction.
    $now_date = new DrupalDateTime();
    $start_date = $auction_dates->start_date;
    $end_date = $auction_dates->end_date;
    if ($now_date instanceof DrupalDateTime && $start_date instanceof DrupalDateTime && $end_date instanceof DrupalDateTime) {
      [$interval_s, $interval_e] = [
        $start_date->diff($now_date),
        $now_date->diff($end_date),
      ];
      if (($interval_s->invert === 1) || ($interval_e->invert === 1)) {
        return FALSE;
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requestAuctionDates(AuctionInterface $auction) : array {
    return [
      'auction_dates' => $auction->get('auction_dates')->first(),
    ];
  }

  /**
   * Get allowed bid step types.
   *
   * @return array
   *   The key/value pairs of valid bid step types.
   */
  public static function getAllowedBidStepTypes() : array {
    $types = [
      'fixed_amount' => t('Last bid with fixed amount'),
      'percentage_initial' => t('Last bid with percentage of initial amount'),
      'percentage_last' => t('Last bid with percentage of its amount'),
    ];

    return $types;
  }

  /**
   * Helper function to calculate max/min amount for an incoming bid.
   *
   * @param string $step_type
   *   The step type.
   * @param string $step_value
   *   The per-auction defined value to validate each incoming bid.
   * @param string $auction_initial_amount
   *   The per-auction defined initial amount.
   * @param string $base_bid_amount
   *   The given (incoming) bid amount, or value to check against to.
   * @param string $direction
   *   One of "ascending" or "descending", depending nature of auction plugin.
   *
   * @return string
   *   The max/min required value.
   */
  public function getNetxRequiredBidAmount(string $step_type, string $step_value, string $auction_initial_amount, string $base_bid_amount, string $direction) {
    if (!in_array($direction, ['ascending', 'descending'])) {
      throw new \InvalidArgumentException(sprintf('Invalid sort option %s provided. It must be "ascending" or "descending".', $direction));
    }
    if ($step_type == 'fixed_amount') {
      return $direction == 'ascending' ? bcadd($base_bid_amount, $step_value) : bcsub($base_bid_amount, $step_value);
    }
    elseif ($step_type == 'percentage_initial') {
      $percentage = bcdiv(bcmul($auction_initial_amount, $step_value), 100);

      return $direction == 'ascending' ? bcadd($base_bid_amount, $percentage) : bcsub($base_bid_amount, $percentage);
    }
    elseif ($step_type == 'percentage_last') {
      $percentage = bcdiv(bcmul($base_bid_amount, $step_value), 100);

      return $direction == 'ascending' ? bcadd($base_bid_amount, $percentage) : bcsub($base_bid_amount, $percentage);
    }
    else {
      throw new \InvalidArgumentException(sprintf('Invalid step type %s provided. It must be one of "fixed_amount", "percentage_initial" or "percentage_last".', $step_type));
    }
  }

  /**
   * Helper method to get next required bid amount, given an auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction content entity.
   * @param string $direction
   *   The string direction telling if next value should be lower or upper.
   */
  public function getNetxRequiredBidAmountByAuction(AuctionInterface $auction, string $direction) {
    // Last bid, if exists, to do our validations.
    $last_bid = $auction->getLastBid();
    // Amout validations.
    $initial_amount = $auction->get('auction_initial_amount')->getString();
    $step_type = $auction->get('auction_bid_step_type')->getString();
    $step_value = $auction->get('auction_bid_step_value')->getString();
    if ($last_bid) {
      // If there is a previous bid.
      $base_bid_amount = $last_bid->get('bid_amount')->getString();
    }
    else {
      // If there isn't any previous bid.
      $base_bid_amount = $initial_amount;
    }
    $required_bid_amount = $this->getNetxRequiredBidAmount($step_type, $step_value, $initial_amount, $base_bid_amount, $direction);

    return $required_bid_amount;
  }

  /**
   * Compares 2 values.
   *
   * @param string $left_operand
   *   The first value to compare.
   * @param string $right_operand
   *   The second value to compare.
   * @param int $scale
   *   The scale to be used.
   *
   * @return int
   *   The comparison result as returned by BCMath.
   */
  public function compareAmounts(string $left_operand, string $right_operand, int $scale = 0) {
    return bccomp($left_operand, $right_operand, $scale);
  }

}
