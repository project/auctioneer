<?php

namespace Drupal\auctioneer_datetime_range\Commands;

use Drush\Commands\DrushCommands as DrushCommandsBase;
use Drupal\auctioneer\AuctionTypeManagerInterface;
use Drupal\auctioneer_datetime_range\Batch\CheckAuctionDatesBatch;

/**
 * Defines Drush commands.
 */
class DrushCommands extends DrushCommandsBase {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager) {
    parent::__construct();
    $this->auctionTypeManager = $auction_type_manager;
  }

  /**
   * Close the auctions that are no longer into a valid date range.
   *
   * @command auctioneer:check_auction_dates
   *
   * @option limit
   *   The amount of bids to process at time.
   *
   * @usage drush auctioneer:check_auction_dates
   *   Check auction dates. Basic usage.
   * @usage drush auctioneer:check_auction_dates --limit=xxx
   *   Check auction dates. Process an amount of xxx auctions.
   *
   * @aliases auctioneer-cad
   */
  public function checkAuctionDates(
    array $options = [
      'limit' => 30,
    ]
  ) {
    batch_set(CheckAuctionDatesBatch::getBatchDefinition($options));
    drush_backend_batch_process();
  }

}
