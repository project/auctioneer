<?php

namespace Drupal\auctioneer_datetime_range;

use Drupal\auctioneer\Entity\AuctionInterface;

/**
 * Defines the interface DatetimeRangeAuctionTypeInterface.
 */
interface DatetimeRangeAuctionTypeInterface {

  /**
   * The context key for auction types implementing this interface.
   *
   * @string
   */
  const AUCTION_TYPE_CONTEXT_KEY = 'datetime_range_auction_type';

  /**
   * Helper function to determine if an auction is into a valid date range.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction content entity.
   *
   * @return bool
   *   Boolean telling if current date is into the auction dates.
   */
  public function auctionIsInDateRange(AuctionInterface $auction) : bool;

  /**
   * Request auction dates.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction content entity.
   *
   * @return array
   *   The date items for this auction.
   *
   * @example
   *   field_name => \Drupal\Core\Datetime\DrupalDateTime
   *   field_name => [
   *     delta => \Drupal\Core\Datetime\DrupalDateTime
   *   ]
   */
  public function requestAuctionDates(AuctionInterface $auction) : array;

}
