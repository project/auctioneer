<?php

namespace Drupal\auctioneer_datetime_range\Batch;

use Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeInterface;

/**
 * Provides managing service for auction datetime range.
 */
class CheckAuctionDatesBatch {

  /**
   * {@inheritdoc}
   */
  public static function getBatchDefinition(array $params = []) {
    $batch = [
      'title' => t('Processing datetime range auctions...'),
      'operations' => [
        [
          '\Drupal\auctioneer_datetime_range\Batch\CheckAuctionDatesBatch::checkAuctionDatesProcess',
          [$params],
        ],
      ],
      'finished' => '\Drupal\auctioneer_datetime_range\Batch\CheckAuctionDatesBatch::checkAuctionDatesFinished',
    ];

    return $batch;
  }

  /**
   * {@inheritdoc}
   */
  public static function checkAuctionDatesProcess(array $params, &$context) {
    if (empty($context['sandbox'])) {
      $auction_type_context_key = DatetimeRangeAuctionTypeInterface::AUCTION_TYPE_CONTEXT_KEY;
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['offset'] = 0;
      $context['sandbox']['max'] = \Drupal::database()
        ->query("
          SELECT COUNT(DISTINCT a.id) FROM {auction} a
          INNER JOIN {auctioneer_auction_statistics} s ON a.id = s.auction_id
          INNER JOIN {auction_field_data} d ON a.id = d.id
          WHERE s.context LIKE '%$auction_type_context_key%'
          AND d.auction_state != 'closed'
        ")
        ->fetchField();
      // Initializing collectors.
      $context['results']['acquitted'] = [];
      $context['results']['processed'] = [];
    }
    $data = \Drupal::service('auctioneer_datetime_range.auction_type.manager')->checkDatetimeRangeAuctionTypeAuctions([
      'offset' => $context['sandbox']['offset'],
      'limit' => $params['limit'] ?? 30,
    ]);
    if (isset($data['acquitted']) && !empty($data['acquitted'])) {
      // If there are open auctions, we need to skip them at next iteration.
      $context['sandbox']['offset'] += count($data['acquitted']);
      $context['results']['acquitted'] += $data['acquitted'];
    }
    if (isset($data['processed']) && !empty($data['processed'])) {
      $context['results']['processed'] += $data['processed'];
    }
    if (isset($data['_auctions']) && !empty($data['_auctions'])) {
      $context['sandbox']['progress'] += count($data['_auctions']);
    }
    else {
      $context['finished'] = 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function checkAuctionDatesFinished($success, $results, $operations) {
    if ($success) {
      $processed = $acquitted = 0;
      if (is_array($results)) {
        $processed = isset($results['processed']) ? count($results['processed']) : 0;
        $acquitted = isset($results['acquitted']) ? count($results['acquitted']) : 0;
      }
      $checked = $processed + $acquitted;
      $message = t('Job has been completed successfully. Checked: @checked; Processed: @processed; Acquitted: @acquitted.', [
        '@checked' => $checked,
        '@processed' => $processed,
        '@acquitted' => $acquitted,
      ]);
    }
    else {
      $message = t('There was an error trying to process date checking. Check logs for more information.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
