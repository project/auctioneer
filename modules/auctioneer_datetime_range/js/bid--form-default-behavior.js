/**
 * @file
 * Bid form behavior.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the auctioneerBidForm behavior.
   */
  Drupal.behaviors.auctioneerBidDefaultForm = {
    attach: function (context) {
      $('.js--auction-place-bid').each(function () {
        var $form = $(this).find('form');
        if (typeof $form !== 'undefined' && !$(this).hasClass('js--auctioneer-place-bid-processed')) {
          var nextBidAmount = $form.data('next-bid');
          if (typeof nextBidAmount !== 'undefined' && nextBidAmount != '') {
            var $amountField = $form.find('.field--name-bid-amount input[type="text"]');
            var $submit = $form.find('.js--bid-submit');
            if ($amountField.length > 0 && $submit.length > 0) {
              $amountField.val(nextBidAmount);
              $amountField.closest('.field--name-bid-amount').hide();
              // Let's tweak submit.
              $submit.hide();
              $submit.parent().prepend('<button class="js--bid-amount-trigger"><span>' + Drupal.t('Bid $@value', {'@value': nextBidAmount}) + '</span></button>');
              $submit.parent().prepend('<a href="#" class="js--bid-amount-custom">' + Drupal.t('Custom amount') + '</a>');
            }
          }
        }
        $(this).addClass('js--auctioneer-place-bid-processed');
      });
      // Click behavior for custom bid amount.
      $(document).on('click', '.js--bid-amount-custom', function (event) {
        event.preventDefault();
        var $form = $(this).closest('form');
        var $submit = $form.find('.js--bid-submit');
        $submit.show();
        $form.find('.field--name-bid-amount').show();
        $form.find('.js--bid-amount-trigger').hide();
        $(this).hide();
      });
      // Click behavior for custom bid button.
      $(document).on('click', '.js--bid-amount-trigger', function (event) {
        event.preventDefault();
        var $submit = $(this).closest('form').find('.js--bid-submit');
        $submit.trigger('click');
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
