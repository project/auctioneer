<?php

namespace Drupal\auctioneer_commerce\Event;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the bid add to cart event.
 */
class BidAddToCartEvent extends Event {

  const EVENT_NAME = 'auctioneer.bid.event.add_to_cart';

  /**
   * The bid entity to be processed.
   *
   * @var \Drupal\auctioneer\Entity\BidInterface
   */
  protected $bid;

  /**
   * The order item entity to be processed.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  protected $orderItem;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid object.
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item object.
   */
  public function __construct(BidInterface $bid, OrderItemInterface $order_item) {
    $this->bid = $bid;
    $this->orderItem = $order_item;
  }

  /**
   * Get bid object.
   */
  public function bid() {
    return $this->bid;
  }

  /**
   * Get order item object.
   */
  public function orderItem() {
    return $this->orderItem;
  }

}
