<?php

namespace Drupal\auctioneer_commerce;

use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer\AuctionTypeManagerInterface;

/**
 * Provides managing service for auction commerce integration.
 */
interface AuctionTypeCommerceManagerInterface {

  /**
   * Returns the top auction type manager.
   */
  public function auctionTypeManager() : AuctionTypeManagerInterface;

  /**
   * Initialize required commerce configurations for a given auction type.
   *
   * @param \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type
   *   The auction type entity object.
   * @param bool $lock_fields
   *   Boolean telling if fields should be locked.
   */
  public function initializeAuctionTypeCommerceConfigurations(AuctionTypeInterface $auction_type, bool $lock_fields = TRUE);

  /**
   * Check if commerce settings are enabled.
   *
   * @param \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type
   *   The auction type entity object.
   */
  public function isCommerceEnabled(AuctionTypeInterface $auction_type) : bool;

  /**
   * Get required purchasig entities from bid.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid entity.
   */
  public function getPurchasingEntitiesFromBid(BidInterface $bid) : array;

  /**
   * Get integration settings given an auction entity.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction entity.
   */
  public function getCommerceSettingsFromAuction(AuctionInterface $auction) : array;

  /**
   * Get integration settings given a bid entity.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid entity.
   */
  public function getCommerceSettingsFromBid(BidInterface $bid) : array;

  /**
   * Add bid to the cart.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid entity.
   * @param array $params
   *   Extra parameters.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemInterface|null
   *   NULL or the order item if added successfully.
   */
  public function addBidToCart(BidInterface $bid, array $params = []);

}
