<?php

namespace Drupal\auctioneer_commerce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer_commerce\AuctionTypeCommerceManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the commerce integration remove form.
 */
class RemoveSettingsForm extends FormBase {

  /**
   * The auction type object.
   *
   * @var \Drupal\auctioneer\Entity\AuctionTypeInterface
   */
  protected $auctionType;

  /**
   * The auctioneer commerce manager.
   *
   * @var \Drupal\auctioneer_commerce\AuctionTypeCommerceManagerInterface
   */
  protected $commerceManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(AuctionTypeCommerceManagerInterface $commerce_manager, RouteMatchInterface $route_match) {
    $this->commerceManager = $commerce_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('auctioneer_commerce.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_commerce_remove_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->auctionType = $this->routeMatch->getParameter('auction_type');
    if ($this->commerceManager->isCommerceEnabled($this->auctionType)) {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Commerce field data and configurations for the auction type <em>@auction_type_label</em> will be dropped. Drop integration?', [
          '@auction_type_label' => $this->auctionType->label(),
        ]),
      ];
      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t("Drop integration"),
        '#button_type' => 'primary',
      ];
    }
    else {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("Seems there isn't any Dupal commerce integration for this auction type yet."),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Processing auction type <em>@auction_type_label</em>...', ['@auction_type_label' => $this->auctionType->label()]),
      'operations' => [
        [
          '\Drupal\auctioneer_commerce\Form\RemoveSettingsForm::removeAuctionTypeData',
          [$this->auctionType],
        ],
        [
          '\Drupal\auctioneer_commerce\Form\RemoveSettingsForm::removeAuctionTypeConfigurations',
          [$this->auctionType],
        ],
      ],
      'finished' => '\Drupal\auctioneer_commerce\Form\RemoveSettingsForm::auctionTypeProcessingFinished',
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public static function removeAuctionTypeData(AuctionTypeInterface $auction_type, &$context) {
    if (empty($context['sandbox'])) {
      $context['results']['auction_type'] = $auction_type;
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = \Drupal::database()
        ->query('SELECT COUNT(DISTINCT id) FROM {auction} WHERE bundle = :bundle', [':bundle' => $auction_type->id()])
        ->fetchField();
    }
    $limit = 15;
    $result = \Drupal::database()
      ->select('auction')
      ->fields('auction', ['id'])
      ->condition('id', $context['sandbox']['current_id'], '>')
      ->condition('bundle', $auction_type->id())
      ->orderBy('id')
      ->range(0, $limit)
      ->execute()
      ->fetchCol();
    $auctions = !empty($result) ? \Drupal::entityTypeManager()->getStorage('auction')->loadMultiple($result) : [];
    if (!empty($auctions)) {
      foreach ($auctions as $auction) {
        // Needed to clean-up fields before removing.
        $auction->set('auction_commerce_store', NULL);
        $auction->set('auction_commerce_currency', NULL);
        $auction->save();
        // Progress stuff.
        $context['results']['id'][] = $auction->id();
        $context['sandbox']['progress']++;
        $context['sandbox']['current_id'] = $auction->id();
      }
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
    else {
      $context['finished'] = 1;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function removeAuctionTypeConfigurations(AuctionTypeInterface $auction_type, &$context) {
    foreach (['auction_commerce_store', 'auction_commerce_currency'] as $field_name) {
      $field_config = FieldConfig::loadByName('auction', $auction_type->id(), $field_name);
      if ($field_config) {
        $field_config->delete();
      }
    }
    $auction_type->unsetThirdPartySetting('auctioneer_commerce', 'settings');
    $auction_type->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function auctionTypeProcessingFinished($success, $results, $operations) {
    if ($success) {
      $message = t('Auction type <em>@auction_type_label</em> was successfully updated. Updated auctions: @total_updated', [
        '@auction_type_label' => $results['auction_type']->label(),
        '@total_updated' => isset($results['id']) ? count($results['id']) : 0,
      ]);
    }
    else {
      $message = t('There was an error trying to remove integration for the selected auction type. Check logs for more information.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
