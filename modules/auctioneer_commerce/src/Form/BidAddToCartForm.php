<?php

namespace Drupal\auctioneer_commerce\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_order\Form\CustomerFormTrait;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer_commerce\AuctionTypeCommerceManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the add to cart form to the winner's one.
 */
class BidAddToCartForm extends FormBase {

  use CustomerFormTrait;

  /**
   * The bid object.
   *
   * @var \Drupal\auctioneer\Entity\BidInterface
   */
  protected $bid;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The auctioneer commerce manager.
   *
   * @var \Drupal\auctioneer_commerce\AuctionTypeCommerceManagerInterface
   */
  protected $commerceManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(RouteMatchInterface $route_match, AuctionTypeCommerceManagerInterface $commerce_manager) {
    $this->userStorage = $commerce_manager->auctionTypeManager()->entityTypeManager()->getStorage('user');
    // Our needed elements itself.
    $this->routeMatch = $route_match;
    $this->commerceManager = $commerce_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('auctioneer_commerce.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_commerce_add_to_cart_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $this->routeMatch->getParameter('bid');
    $items = $bid ? $this->commerceManager->getPurchasingEntitiesFromBid($bid) : [];
    if (!isset($items['auction']) || !$items['auction'] instanceof AuctionInterface) {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Current bid is not attached to any valid auction.'),
        '#attributes' => [
          'class' => [
            'auction-warning',
            'auction-warning--bid-orphan',
          ],
        ],
      ];

      return $form;
    }
    $settings = $this->commerceManager->getCommerceSettingsFromAuction($items['auction']);
    if (!isset($settings['enable']) || !(bool) $settings['enable']) {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("Current auction type is not enabled to add bids to the owner's cart."),
        '#attributes' => [
          'class' => [
            'auction-warning',
            'auction-warning--auction-type-disabled',
          ],
        ],
      ];

      return $form;
    }
    if (isset($items['store']) && $items['store'] && isset($items['currency']) && $items['currency']) {
      $this->bid = $bid;
      $form = $this->buildBidCustomerForm($form, $form_state);
      $form['summary'] = [
        '#type' => 'view',
        '#name' => 'auctioneer_commerce_cart_summary',
        '#arguments' => [$this->bid->id()],
        '#embed' => TRUE,
        '#prefix' => '<div class="bid-cart-summary">',
        '#suffix' => '</div>',
        '#weight' => -5,
      ];
      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t("Add to the owner's cart"),
        '#button_type' => 'primary',
      ];
      if (isset($settings['cart_ajax']) && (bool) $settings['cart_ajax']) {
        $form['ajax_output'] = [
          '#prefix' => '<div class="ajax-output--bid-' . $this->bid->id() . '">',
          '#suffix' => '</div>',
          '#tree' => FALSE,
          '#weight' => -10,
        ];
        $form['actions']['submit']['#ajax'] = [
          'callback' => [$this, 'ajaxCallback'],
        ];
      }
      $form['#access'] = isset($settings['enable']) && (bool) $settings['enable'];
    }
    else {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Current auction does not have any configured Store or Currency settings.'),
        '#attributes' => [
          'class' => [
            'auction-warning',
            'auction-warning--missing-params',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $request = $this->getRequest();
    $response = new AjaxResponse();
    $form['ajax_output']['messages'] = [
      '#type' => 'status_messages',
      '#message_list' => $this->messenger()->all(),
    ];
    $remove_customer_form_command = $disable_submit_form_command = NULL;
    if (!$form_state->hasAnyErrors()) {
      $form['ajax_output']['redirect'] = [
        '#theme' => 'auctioneer_commerce_delay_redirect',
        '#path' => $request->query->get('destination') ?? '',
        '#delay' => 3000,
      ];
      if (isset($form['customer'])) {
        $remove_customer_form_command = new ReplaceCommand('.form-customer--bid-' . $this->bid->id(), '');
      }
      $disable_submit_form_command = new InvokeCommand('.form-actions input', 'disabled', [TRUE]);
    }
    // Let's merge all commands in right order.
    $response->addCommand(new ReplaceCommand('.ajax-output--bid-' . $this->bid->id(), $form['ajax_output']));
    if ($remove_customer_form_command) {
      $response->addCommand($remove_customer_form_command);
    }
    if ($disable_submit_form_command) {
      $response->addCommand($disable_submit_form_command);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->bid) {
      $hammer = $this->commerceManager->auctionTypeManager()->getAuctionHammerBidData($this->bid);
      if (empty($hammer)) {
        $form_state->setError($form['actions']['submit'], $this->t('Provided bid is invalid.'));
      }
      if (isset($form['customer'])) {
        $this->validateBidCustomerForm($form, $form_state);
      }
    }
    else {
      $form_state->setError($form['actions']['submit'], $this->t('You must to provide a hammer bid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->bid instanceof BidInterface) {
      if (isset($form['customer'])) {
        $this->submitBidCustomerForm($form, $form_state);
      }
      $values = $form_state->getValues();
      $params = [];
      if ($cart_owner = isset($values['uid']) ? $this->userStorage->load($values['uid']) : NULL) {
        $params['customer'] = $cart_owner;
      }
      // Now, let's add the bit to the cart.
      if ($this->commerceManager->addBidToCart($this->bid, $params)) {
        $this->messenger()->addMessage($this->t("Bid was successfully added to the owner's cart."));
      }
      else {
        $this->messenger()->addMessage($this->t("Can't add to the owner's cart. Check logs for more details."), 'error');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildBidCustomerForm(array $form, FormStateInterface $form_state) {
    $items = $this->bid ? $this->commerceManager->getPurchasingEntitiesFromBid($this->bid) : [];
    if (isset($items['auction']) && $items['auction']) {
      $settings = $this->commerceManager->getCommerceSettingsFromAuction($items['auction']);
      $uid = isset($items['customer']) && $items['customer'] ? $items['customer']->id() : 0;
      if ($uid > 0 && isset($settings['different_customer']) && (bool) $settings['different_customer']) {
        $form = $this->buildCustomerForm($form, $form_state);
        if (isset($form['customer']) && isset($form['customer']['customer_type'])) {
          $form['customer']['customer_type']['#options']['bid_owner'] = $this->t("Bid's owner");
          if (!$form_state->getTriggeringElement()) {
            $form['customer']['customer_type']['#default_value'] = 'bid_owner';
            $form_state->setValue('customer_type', 'bid_owner');
          }
          if ($form_state->getValue('customer_type') == 'bid_owner') {
            $form['customer']['uid'] = [
              '#type' => 'value',
              '#value' => $uid,
            ];
            $form['customer']['view'] = [
              '#type' => 'view',
              '#name' => 'auctioneer_commerce_cart_customer',
              '#arguments' => [$uid],
              '#embed' => TRUE,
              '#prefix' => '<div class="bid-cart-customer">',
              '#suffix' => '</div>',
            ];
            unset($form['customer']['mail']);
            unset($form['customer']['password']);
            unset($form['customer']['notify']);
          }
        }
      }
      elseif (!($uid > 0)) {
        $form = $this->buildCustomerForm($form, $form_state);
      }
      else {
        $form = [
          'uid' => [
            '#type' => 'value',
            '#value' => $uid,
          ],
          'user' => [
            '#type' => 'fieldset',
            '#title' => $this->t('Customer'),
            'view' => [
              '#type' => 'view',
              '#name' => 'auctioneer_commerce_cart_customer',
              '#arguments' => [$uid],
              '#embed' => TRUE,
              '#prefix' => '<div class="bid-cart-customer">',
              '#suffix' => '</div>',
            ],
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateBidCustomerForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($values['customer_type']) && $values['customer_type'] != 'bid_owner') {
      $this->validateCustomerForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitBidCustomerForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['customer_type'] != 'bid_owner') {
      $this->submitCustomerForm($form, $form_state);
    }
  }

}
