<?php

namespace Drupal\auctioneer_commerce;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\AuctionTypeManagerInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer_commerce\Event\BidAddToCartEvent;
use Drupal\user\UserInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\Entity\CurrencyInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides managing service for auction commerce integration.
 */
class AuctionTypeCommerceManager implements AuctionTypeCommerceManagerInterface {

  use StringTranslationTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface
   */
  protected $orderTypeResolver;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\commerce_order\Resolver\OrderTypeResolverInterface $order_type_resolver
   *   The order type resolver.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    AuctionTypeManagerInterface $auction_type_manager,
    CartManagerInterface $cart_manager,
    CartProviderInterface $cart_provider,
    OrderTypeResolverInterface $order_type_resolver,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->auctionTypeManager = $auction_type_manager;
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
    $this->orderTypeResolver = $order_type_resolver;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function auctionTypeManager() : AuctionTypeManagerInterface {
    return $this->auctionTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeAuctionTypeCommerceConfigurations(AuctionTypeInterface $auction_type, bool $lock_fields = TRUE) {
    $settings = $auction_type->getThirdPartySetting('auctioneer_commerce', 'settings');
    if (empty($settings) || !isset($settings['bid_amount_field_name']) || !isset($settings['enable']) || (isset($settings['enable']) && !(bool) $settings['enable'])) {
      return;
    }
    $fields = [
      'auction_commerce_store' => BaseFieldDefinition::create('entity_reference')
        ->setLabel($this->t('Store'))
        ->setDescription($this->t('The auction store.'))
        ->setCardinality(1)
        ->setRequired(TRUE)
        ->setSetting('target_type', 'commerce_store')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('form', [
          'type' => 'commerce_entity_select',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
      'auction_commerce_currency' => BaseFieldDefinition::create('entity_reference')
        ->setLabel($this->t('Currency'))
        ->setDescription($this->t('The auction currency.'))
        ->setCardinality(1)
        ->setRequired(TRUE)
        ->setSetting('target_type', 'commerce_currency')
        ->setSetting('handler', 'default')
        ->setTranslatable(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE),
    ];
    $auction_field_manager = $this->auctionTypeManager->auctionFieldManager();
    $auction_field_manager->provideEntityTypeFields($fields, 'auction', $auction_type->id(), $lock_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function isCommerceEnabled(AuctionTypeInterface $auction_type) : bool {
    $enabled = FALSE;
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('auction', $auction_type->id());
    if (isset($field_definitions['auction_commerce_store']) || isset($field_definitions['auction_commerce_currency'])) {
      $enabled = TRUE;
    }

    return $enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasingEntitiesFromBid(BidInterface $bid) : array {
    $auction = $bid->getAuction();
    $customer = $bid->getOwner();
    $seller = NULL;
    $store = NULL;
    $currency = NULL;
    if ($auction instanceof AuctionInterface) {
      $seller = $auction->getOwner();
      $store = $auction->hasField('auction_commerce_store') ? $auction->get('auction_commerce_store')->entity : NULL;
      $currency = $auction->hasField('auction_commerce_currency') ? $auction->get('auction_commerce_currency')->entity : NULL;
    }

    return [
      'auction' => $auction,
      'seller' => $seller,
      'customer' => $customer,
      'store' => $store,
      'currency' => $currency,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCommerceSettingsFromAuction(AuctionInterface $auction) : array {
    $settings = $auction->getAuctionType()->getThirdPartySetting('auctioneer_commerce', 'settings');

    return is_array($settings) ? $settings : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCommerceSettingsFromBid(BidInterface $bid) : array {
    $settings = [];
    $auction = $bid->getAuction();
    if ($auction instanceof AuctionInterface) {
      $settings = $this->getCommerceSettingsFromAuction($auction);
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function addBidToCart(BidInterface $bid, array $params = []) {
    $items = $this->getPurchasingEntitiesFromBid($bid);
    if (isset($params['customer']) && $params['customer'] instanceof UserInterface) {
      $items['customer'] = $params['customer'];
    }
    if (
      isset($items['auction']) &&
      $items['auction'] instanceof AuctionInterface &&
      isset($items['store']) &&
      $items['store'] instanceof StoreInterface &&
      isset($items['currency']) &&
      $items['currency'] instanceof CurrencyInterface &&
      isset($items['customer']) &&
      $items['customer'] instanceof UserInterface
    ) {
      $settings = $this->getCommerceSettingsFromAuction($items['auction']);
      if (
        isset($settings['enable']) &&
        (bool) $settings['enable'] &&
        isset($settings['bid_amount_field_name']) &&
        $bid->hasField($settings['bid_amount_field_name'])
      ) {
        $_currency_code = $items['currency']->getCurrencyCode();
        $_currency_number = $bid->get($settings['bid_amount_field_name'])->first()->getString();
        $_commerce_price = $_currency_number && $_currency_code ? new Price($_currency_number, $_currency_code) : NULL;
        if ($_commerce_price) {
          $order_item = $this->auctionTypeManager->entityTypeManager()->getStorage('commerce_order_item')->create([
            'type' => 'auctioneer_commerce_auction',
            'quantity' => isset($params['quantity']) && is_numeric($params['quantity']) ? $params['quantity'] : 1,
            'unit_price' => $_commerce_price,
            'title' => isset($params['title']) && !empty($params['title']) ? $params['title'] : $items['auction']->label(),
            'auction_bid' => [
              'target_id' => $bid->id(),
            ],
          ]);
          $_order_type_id = $this->orderTypeResolver->resolve($order_item);
          $cart = $this->cartProvider->getCart($_order_type_id, $items['store'], $items['customer']);
          if (!$cart) {
            $cart = $this->cartProvider->createCart($_order_type_id, $items['store'], $items['customer']);
          }
          if ($order_item = $this->cartManager->addOrderItem($cart, $order_item)) {
            $event = new BidAddToCartEvent($bid, $order_item);
            $this->eventDispatcher->dispatch($event, BidAddToCartEvent::EVENT_NAME);

            return $order_item;
          }
        }
        else {
          $this->auctionTypeManager->logger('auctioneer_commerce')->error(
            'Error trying to add bid ID <em>%bid_id</em> to cart. Missing or wrong values to provide a product price: <em>%currency_code</em>$<em>%price_value</em>',
            [
              '%bid_id' => $bid->id(),
              '%currency_code' => $_currency_code,
              '%price_value' => $_currency_number,
            ]
          );
        }
      }
      else {
        $this->auctionTypeManager->logger('auctioneer_commerce')->error(
          'Error trying to add bid ID <em>%bid_id</em> to cart. The provided bid is invalid or field <em>%field_name</em> is not available.',
          [
            '%bid_id' => $bid->id(),
            '%field_name' => $settings['bid_amount_field_name'],
          ]
        );
      }
    }
    else {
      $this->auctionTypeManager->logger('auctioneer_commerce')->error(
        'Error trying to add bid ID <em>%bid_id</em> to cart. Check the parent auction to see if all needed values such as the store and currency were provided.',
        [
          '%bid_id' => $bid->id(),
        ]
      );
    }
  }

}
