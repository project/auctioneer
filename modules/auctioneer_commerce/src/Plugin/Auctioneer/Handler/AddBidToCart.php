<?php

namespace Drupal\auctioneer_commerce\Plugin\Auctioneer\Handler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\auctioneer\Handler;
use Drupal\auctioneer\Entity\BidInterface;

/**
 * Provides the "AddBidToCart" handler plugin.
 *
 * @Handler(
 *   id = "auctioneer_handler_add_bid_to_cart",
 *   label = "Add bid to cart",
 *   description = "Add specified bid to the cart.",
 *   scope = "bid, auction"
 * )
 */
class AddBidToCart extends Handler {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The auction commerce manager.
   *
   * @var \Drupal\auctioneer_commerce\AuctionTypeCommerceManagerInterface
   */
  protected $auctionCommerceManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->token = $container->get('token');
    $instance->auctionCommerceManager = $container->get('auctioneer_commerce.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $configuration = $this->getConfiguration();
    $global_settings = $this->getGlobalModuleSettings();
    $_rl = isset($global_settings['token']['recursion_limit']) && is_numeric($global_settings['token']['recursion_limit']) ? $global_settings['token']['recursion_limit'] : 3;
    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        $entity->getEntityType()->getBundleOf(),
      ],
      '#recursion_limit' => $_rl,
    ];
    $form['bid_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bid ID'),
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['item']['bid_id']['value'] ?? '',
      '#description' => $this->t('The winner bid ID.'),
    ];
    $form['owner_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Owner ID'),
      '#default_value' => $configuration['settings']['item']['owner_id']['value'] ?? '',
      '#description' => $this->t("The commerce cart owner. If this value is not provided, then the bid will be added to the bid owner's cart."),
    ];
    $form['quantity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quantity'),
      '#default_value' => $configuration['settings']['item']['quantity']['value'] ?? '',
      '#description' => $this->t('Amount of items added to the cart. If no value is given, then a default of one item will be added.'),
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order item title'),
      '#default_value' => $configuration['settings']['item']['title']['value'] ?? '',
      '#description' => $this->t('If needed, you can to override the order item title.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $values = $form_state->getValues();
    $this->configuration = [
      'item' => [
        'bid_id' => [
          'value' => $values['bid_id'],
        ],
        'owner_id' => [
          'value' => $values['owner_id'],
        ],
        'quantity' => [
          'value' => $values['quantity'],
        ],
        'title' => [
          'value' => $values['title'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity, array $context = []) {
    $configuration = $this->getConfiguration();
    $global_settings = $this->getGlobalModuleSettings();
    $_replacements = [
      $entity->getEntityTypeId() => $entity,
    ];
    $_options = [
      'clear' => isset($global_settings['token']['clear']) ? $global_settings['token']['clear'] : TRUE,
    ];
    foreach ($configuration['settings']['item'] as $key => $element) {
      if (!empty($element['value'])) {
        $configuration['settings']['item'][$key]['processed'] = $this->token->replace($element['value'], $_replacements, $_options);
      }
    }
    if (
      isset($configuration['settings']['item']['bid_id']['processed']) &&
      is_numeric($configuration['settings']['item']['bid_id']['processed'])
    ) {
      // Just in case. To avoid reloading same bid.
      if ($entity->getEntityTypeId() == 'bid' && $entity->id() == $configuration['settings']['item']['bid_id']['processed']) {
        $bid = $entity;
      }
      else {
        $bid = $this->entityTypeManager->getStorage('bid')->load($configuration['settings']['item']['bid_id']['processed']);
      }
      if ($bid instanceof BidInterface) {
        $params = [];
        if (isset($configuration['settings']['item']['quantity']['processed'])) {
          $params['quantity'] = $configuration['settings']['item']['quantity']['processed'];
        }
        if (isset($configuration['settings']['item']['owner_id']['processed'])) {
          $params['customer'] = $this->entityTypeManager->getStorage('user')->load($configuration['settings']['item']['owner_id']['processed']);
        }
        if (isset($configuration['settings']['item']['title']['processed'])) {
          $params['title'] = $configuration['settings']['item']['title']['processed'];
        }
        // Now, calling method to add to the cart.
        $this->auctionCommerceManager->addBidToCart($bid, $params);
      }
    }
  }

  /**
   * Get global module settings.
   */
  public function getGlobalModuleSettings() {
    return $this->auctionCommerceManager->auctionTypeManager()->configFactory()->get('auctioneer.settings.global')->getRawData();
  }

}
