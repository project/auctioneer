<?php

namespace Drupal\auctioneer_commerce\Plugin\views\area;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Drupal\commerce_price\CurrencyFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a bid amount area.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("auctioneer_commerce_bid_amount")
 */
class BidAmount extends AreaPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The currency formatter.
   *
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    CurrencyFormatter $currency_formatter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['empty']['#description'] = $this->t("Even if selected, this area handler will never render if a valid bid cannot be found in the View's arguments.");
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      $bid_storage = $this->entityTypeManager->getStorage('bid');
      foreach ($this->view->argument as $argument) {
        if (!$argument instanceof NumericArgument) {
          continue;
        }
        if (!in_array($argument->getField(), ['bid_field_data.id'])) {
          continue;
        }
        /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
        if ($bid = $bid_storage->load($argument->getValue())) {
          /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
          if ($auction = $bid->getAuction()) {
            $settings = $auction->getAuctionType()->getThirdPartySetting('auctioneer_commerce', 'settings');
            if (
              !empty($settings) &&
              isset($settings['enable']) &&
              isset($settings['bid_amount_field_name']) &&
              (bool) $settings['enable']
            ) {
              $currency = $auction->get('auction_commerce_currency')->entity;
              if (!$currency || !$bid->hasField($settings['bid_amount_field_name'])) {
                return [];
              }
              $amount = $bid->get($settings['bid_amount_field_name'])->first();
              // Our renderable output.
              $output = [
                '#type' => 'container',
                '#attributes' => [
                  'class' => [
                    'bid-amount',
                  ],
                ],
                'price' => [
                  '#type' => 'html_tag',
                  '#tag' => 'span',
                  '#value' => $this->t('Bid total: @bid_total', [
                    '@bid_total' => $amount ? $this->currencyFormatter->format($amount->getString(), $currency->getCurrencyCode()) : '',
                  ]),
                  '#attributes' => [
                    'class' => [
                      'bid-amount--price',
                    ],
                  ],
                ],
              ];
              return $output;
            }
          }
        }
      }
    }

    return [];
  }

}
