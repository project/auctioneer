<?php

/**
 * @file
 * Install, update and uninstall functions for the auctioneer_commerce module.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_install().
 */
function auctioneer_commerce_install() {
  $order_item_type_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item_type');
  $order_item_type = $order_item_type_storage->load('auctioneer_commerce_auction');
  if (!$order_item_type) {
    // Step 1: Provide needed commerce order item type.
    $order_item_type = $order_item_type_storage->create([
      'id' => 'auctioneer_commerce_auction',
      'label' => t('Auction'),
      'orderType' => 'default',
    ]);
    $order_item_type->save();
    // Step 2: Provide needed fields to the order item type.
    $fields = [
      'auction_bid' => BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Bid'))
        ->setDescription(t('The auction bid.'))
        ->setCardinality(1)
        ->setRequired(TRUE)
        ->setSetting('target_type', 'bid')
        ->setSetting('handler', 'default')
        ->setTranslatable(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE),
    ];
    \Drupal::service('auctioneer.auction_field.manager')->provideEntityTypeFields($fields, 'commerce_order_item', $order_item_type->id());
  }
}
