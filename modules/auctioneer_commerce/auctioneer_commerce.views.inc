<?php

/**
 * @file
 * Provide views data for the auctioneer_commerce module.
 */

/**
 * Implements hook_views_data().
 */
function auctioneer_commerce_views_data() {
  $data['views']['auctioneer_commerce_bid_amount'] = [
    'title' => t('Bid amount'),
    'help' => t('Displays the bid amount, according to the selected field into the auction commerce integration settings.'),
    'area' => [
      'id' => 'auctioneer_commerce_bid_amount',
    ],
  ];

  return $data;
}
