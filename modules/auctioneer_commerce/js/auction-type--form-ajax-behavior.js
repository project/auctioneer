/**
 * @file
 * The "auctionType_ajaxBehavior" behavior.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the auctionType_ajaxBehavior behavior.
   */
  Drupal.behaviors.auctionType_ajaxBehavior = {
    attach: function (context) {

      // This way we keep unaltered parent form function to avoid any conflict.
      $('.form-item-bid-type').on('change', 'select, input', function () {
        $('.form-item-commerce-bid-amount-field-name select').triggerHandler('auctioneer_commerce.auction_type.bid_selection', $(this).val());
      });

    }
  };

})(jQuery, Drupal);
