<?php

namespace Drupal\auctioneer_sniper;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeManagerInterface;
use Drupal\auctioneer_sniper\Event\RequestAuctionLifetimeUpdateEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides managing service for auction sniping operations.
 */
class AuctionSniperManager implements AuctionSniperManagerInterface {

  use StringTranslationTrait;

  /**
   * The datetime auction type manager.
   *
   * @var \Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeManagerInterface
   */
  protected $datetimeAuctionTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeManagerInterface $datetime_auction_type_manager
   *   The auction type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(DatetimeRangeAuctionTypeManagerInterface $datetime_auction_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->datetimeAuctionTypeManager = $datetime_auction_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function datetimeAuctionTypeManager() : DatetimeRangeAuctionTypeManagerInterface {
    return $this->datetimeAuctionTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeAuctionTypeSniperConfigurations(AuctionTypeInterface $auction_type, bool $lock_fields = TRUE) {
    if (!$this->datetimeAuctionTypeManager->auctionTypeIsDatetimeRangeInstance($auction_type)) {
      // Don't allow initializing settings to the unsupported auction types.
      return;
    }
    $fields = [
      'auction_sniper_control_enable' => BaseFieldDefinition::create('boolean')
        ->setLabel($this->t('Enable sniping prevention'))
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
            'display_label' => TRUE,
          ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
      'auction_sniper_threshold_amount' => BaseFieldDefinition::create('integer')
        ->setLabel($this->t('Amount'))
        ->setDisplayOptions('form', [
          'type' => 'number',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
      'auction_sniper_threshold_unit' => BaseFieldDefinition::create('list_string')
        ->setLabel($this->t('Unit'))
        ->setSettings([
          'allowed_values_function' => '\Drupal\auctioneer_sniper\AuctionSniperManager::getSnipingAllowedDateUnits',
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE),
      'auction_sniper_increase_amount' => BaseFieldDefinition::create('integer')
        ->setLabel($this->t('Amount'))
        ->setDisplayOptions('form', [
          'type' => 'number',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE),
      'auction_sniper_increase_unit' => BaseFieldDefinition::create('list_string')
        ->setLabel($this->t('Unit'))
        ->setSettings([
          'allowed_values_function' => '\Drupal\auctioneer_sniper\AuctionSniperManager::getSnipingAllowedDateUnits',
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE),
    ];
    $auction_field_manager = $this->datetimeAuctionTypeManager->auctionTypeManager()->auctionFieldManager();
    $auction_field_manager->provideEntityTypeFields($fields, 'auction', $auction_type->id(), $lock_fields);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSnipingAllowedDateUnits() : array {
    // Does anyone really need seconds?
    return [
      'minutes' => t('Minutes'),
      'hours' => t('Hours'),
      'days' => t('Days'),
      'weeks' => t('Weeks'),
      'months' => t('Months'),
      'years' => t('Years'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function requestAuctionLifetimeUpdate(AuctionInterface $auction) {
    $auction_type = $auction->getAuctionType();
    if (!$this->datetimeAuctionTypeManager->auctionTypeIsDatetimeRangeInstance($auction_type)) {
      return;
    }
    if (
      !$auction->hasField('auction_sniper_control_enable') ||
      !$auction->hasField('auction_sniper_threshold_amount') ||
      !$auction->hasField('auction_sniper_threshold_unit') ||
      !$auction->hasField('auction_sniper_increase_amount') ||
      !$auction->hasField('auction_sniper_increase_unit')
    ) {
      return;
    }
    $settings = $auction_type->getThirdPartySetting('auctioneer_sniper', 'settings');
    $enable = (bool) $auction->get('auction_sniper_control_enable')->getString();
    if (!$enable || (!empty($settings) && isset($settings['enable']) && !(bool) $settings['enable'])) {
      return;
    }
    // Only update if entity has this feature enabled.
    $t_amount = intval($auction->get('auction_sniper_threshold_amount')->getString());
    $t_unit = $auction->get('auction_sniper_threshold_unit')->getString();
    $i_amount = intval($auction->get('auction_sniper_increase_amount')->getString());
    $i_unit = $auction->get('auction_sniper_increase_unit')->getString();
    if ($t_amount && $t_unit && $i_amount && $i_unit) {
      $bid_type = $auction_type->getBidType();
      // Our plugin instance.
      $plugin_instance = $this->datetimeAuctionTypeManager->auctionTypeManager()->auctionPluginManager()->createInstance($bid_type->getPluginName());
      if ($plugin_instance) {
        $now_date = new DrupalDateTime();
        $auction_dates = $plugin_instance->requestAuctionDates($auction);
        $new_values = [];
        foreach ($auction_dates as $field_name => $date_range_items) {
          if (!$auction->hasField($field_name)) {
            continue;
          }
          $date_range_items = is_array($date_range_items) ? $date_range_items : [$date_range_items];
          $delta = 0;
          foreach ($date_range_items as $date_range_item) {
            if ($date_range_item instanceof DateRangeItem) {
              $start_date = $date_range_item->start_date;
              $end_date = $date_range_item->end_date;
              // Calculated auction dates.
              $t_date = clone $end_date;
              $t_date->modify("-$t_amount $t_unit");
              if ($now_date >= $t_date) {
                $i_date = clone $end_date;
                $i_date->modify("+$i_amount $i_unit");
                // Saving new dates into a temporal array, to update later.
                $new_values[$field_name][$delta] = [
                  'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
                  'end_value' => $i_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
                ];
              }
              $delta++;
            }
          }
        }
        // Now, let's set updated dates. We support update by delta.
        if (!empty($new_values)) {
          foreach ($new_values as $field_name => $values) {
            foreach ($values as $delta => $value) {
              $auction->{$field_name}[$delta] = $value;
            }
          }
          $auction->save();
          // Dispatching event.
          $event = new RequestAuctionLifetimeUpdateEvent($auction, $new_values);
          $this->eventDispatcher->dispatch($event, RequestAuctionLifetimeUpdateEvent::EVENT_NAME);
        }
      }
    }

    return $auction;
  }

}
