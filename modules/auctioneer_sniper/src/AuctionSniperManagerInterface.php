<?php

namespace Drupal\auctioneer_sniper;

use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer_datetime_range\DatetimeRangeAuctionTypeManagerInterface;

/**
 * Provides managing service for auction sniper.
 */
interface AuctionSniperManagerInterface {

  /**
   * Returns the datetime auction type manager.
   */
  public function datetimeAuctionTypeManager() : DatetimeRangeAuctionTypeManagerInterface;

  /**
   * Initialize required configurations for a given auction type.
   *
   * @param \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type
   *   The auction type entity object.
   * @param bool $lock_fields
   *   Boolean telling if fields should be locked.
   */
  public function initializeAuctionTypeSniperConfigurations(AuctionTypeInterface $auction_type, bool $lock_fields = TRUE);

  /**
   * Rerturns valid date intervals.
   *
   * @return array
   *   The value|label date intervals.
   */
  public static function getSnipingAllowedDateUnits() : array;

  /**
   * Execute an auction lifetime update.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction entity to be updated.
   *
   * @return null||\Drupal\auctioneer\Entity\AuctionInterface
   *   The updated auction object, if it's valid.
   */
  public function requestAuctionLifetimeUpdate(AuctionInterface $auction);

}
