<?php

namespace Drupal\auctioneer_sniper\Event;

use Drupal\auctioneer\Entity\AuctionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the lifetime update event.
 */
class RequestAuctionLifetimeUpdateEvent extends Event {

  const EVENT_NAME = 'auctioneer.auction.event.lifetime_update';

  /**
   * The auction entity to be processed.
   *
   * @var \Drupal\auctioneer\Entity\AuctionInterface
   */
  protected $auction;

  /**
   * The updated dates.
   *
   * @var array
   */
  protected $requestDates;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction object.
   * @param array $request_dates
   *   The updated datetime items.
   */
  public function __construct(AuctionInterface $auction, array $request_dates) {
    $this->auction = $auction;
    $this->requestDates = $request_dates;
  }

  /**
   * Get auction object.
   */
  public function auction() {
    return $this->auction;
  }

  /**
   * Get updated dates.
   */
  public function requestDates() {
    return $this->requestDates;
  }

}
