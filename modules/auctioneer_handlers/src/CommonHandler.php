<?php

namespace Drupal\auctioneer_handlers;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\auctioneer\Handler;

/**
 * Provides the a common base class for handlers provided in this module.
 */
abstract class CommonHandler extends Handler {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->token = $container->get('token');
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->configFactory = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $_gs = $this->configFactory->get('auctioneer.settings.global')->getRawData();
    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        $entity->getEntityType()->getBundleOf(),
      ],
      '#recursion_limit' => $_gs['token']['recursion_limit'] ?? 3,
    ];

    return $form;
  }

  /**
   * Replaces tokens into the provided string.
   *
   * @param string $value
   *   The string to be processed.
   * @param array $replacements
   *   An array of keyed entities.
   * @param array $options
   *   Extra settings for this replacement.
   *
   * @return string
   *   Value with tokens replaced.
   */
  protected function replaceTokens(string $value, array $replacements, array $options = []) {
    if (!empty($value)) {
      $_options = [
        'clear' => TRUE,
      ] + $options;
      $value = $this->token->replace($value, $replacements, $_options);
    }

    return $value;
  }

  /**
   * Get token options.
   *
   * @param array $override
   *   Custom settings to be overriden.
   */
  protected function prepareTokenOptions(array $override = []) {
    $_gs = $this->configFactory->get('auctioneer.settings.global')->getRawData();
    $options = [
      'clear' => $_gs['token']['clear'] ?? TRUE,
    ] + $override;

    return $options;
  }

  /**
   * Prepare telegram settings before passing to executor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity being processed.
   *
   * @return array
   *   The handler settings values ready to be used.
   */
  public function preparePluginSettings(ContentEntityInterface $entity) : array {
    return $this->getConfiguration();
  }

}
