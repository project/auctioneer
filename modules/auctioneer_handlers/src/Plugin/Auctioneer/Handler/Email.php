<?php

namespace Drupal\auctioneer_handlers\Plugin\Auctioneer\Handler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\auctioneer_handlers\CommonHandler;

/**
 * Provides the "Email" handler plugin.
 *
 * @Handler(
 *   id = "auctioneer_handler_email",
 *   label = "Email",
 *   description = "Send email to certain recipients.",
 *   scope = "bid, auction"
 * )
 */
class Email extends CommonHandler {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->mailManager = $container->get('plugin.manager.mail');
    $instance->emailValidator = $container->get('email.validator');
    $instance->renderer = $container->get('renderer');
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $configuration = $this->getConfiguration();
    $_email_formats = [
      'plain' => $this->t('Plain text email'),
      'html' => $this->t('HTML email'),
    ];
    $form = parent::buildConfigurationForm($form, $form_state, $entity);
    $form['#attributes']['novalidate'] = 'novalidate';
    $form['email_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Email format'),
      '#options' => $_email_formats,
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['email_format'] ?? 'plain',
    ];
    $form['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      '#default_value' => $configuration['settings']['email_content']['from']['value'] ?? '',
      '#description' => $this->t('The email sender. Leave it blank to use default site information.'),
      '#attributes' => [
        'placeholder' => $this->t('Example @email', ['@email' => '<mail@example.site>']),
      ],
    ];
    $form['cc'] = [
      '#type' => 'textfield',
      '#title' => 'CC',
      '#default_value' => $configuration['settings']['email_content']['cc']['value'] ?? '',
      '#description' => $this->t('The CC address to send this message, too. Multiple values may be added, separated by ",".'),
    ];
    $form['bcc'] = [
      '#type' => 'textfield',
      '#title' => 'BCC',
      '#default_value' => $configuration['settings']['email_content']['bcc']['value'] ?? '',
      '#description' => $this->t('The BCC address to send this message, too. Multiple values may be added, separated by ",".'),
    ];
    $form['to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['email_content']['to']['value'] ?? '',
      '#description' => $this->t('The email address to send this message to. Multiple values may be added, separated by ",".'),
    ];
    $form['reply_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply to'),
      '#default_value' => $configuration['settings']['email_content']['reply_to']['value'] ?? '',
      '#attributes' => [
        'placeholder' => $this->t('Example @email', ['@email' => '<mail@example.site>']),
      ],
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $configuration['settings']['email_content']['subject']['value'] ?? '',
      '#description' => $this->t('The email subject of this message.'),
    ];
    $form['message_plain'] = [
      '#type' => 'textarea',
    ];
    $form['message_html'] = [
      '#type' => 'text_format',
      '#format' => $configuration['settings']['email_content']['message_html']['format'] ?? NULL,
    ];
    foreach ($_email_formats as $key => $value) {
      $form['message_' . $key] += [
        '#title' => $this->t('Message'),
        '#default_value' => $configuration['settings']['email_content']['message_' . $key]['value'] ?? '',
        '#description' => $this->t('The @format_label.', ['@format_label' => $value]),
        '#states' => [
          'visible' => [
            'select[name="plugin[settings][email_format]"]' => ['value' => $key],
          ],
          'required' => [
            'select[name="plugin[settings][email_format]"]' => ['value' => $key],
          ],
        ],
      ];
    }
    $_attachments_help = [
      '#type' => 'container',
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $this->t('Enter a comma-separated list of file paths to be attached to this email. If needed, some patterns may be used:'),
      ],
      'examples' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('<strong>@example_0</strong>: Basic pattern without any extra behavior.', ['@example_0' => 'public://inline-files/file.txt']),
          $this->t('<strong>@example_1</strong>: Basic pattern overriding file name.', ['@example_1' => 'public://inline-files/file.txt|Filename.txt']),
          $this->t('<strong>@example_2</strong>: Basic pattern with tokens usage.', ['@example_2' => 'public://inline-files/file-[token-type:token-example].txt|Filename.txt']),
        ],
      ],
    ];
    $form['attachments'] = [
      '#type' => 'textarea',
      '#title' => $this->t('File attachments'),
      '#description' => $this->renderer->renderPlain($_attachments_help),
      '#default_value' => $configuration['settings']['email_content']['attachments']['value'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $values = $form_state->getValues();
    foreach (['cc', 'bcc', 'to'] as $field_name) {
      if (!empty($values[$field_name])) {
        $_items = explode(',', $values[$field_name]);
        $_result = $this->validateEmails($_items, TRUE);
        if (isset($_result['invalid']) && !empty($_result['invalid'])) {
          $form_state->setErrorByName(
            $field_name,
            $this->t(
              'Invalid email values found: @invalid_items',
              [
                '@invalid_items' => implode(', ', $_result['invalid']),
              ]
            )
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $values = $form_state->getValues();
    $this->configuration = [
      'email_format' => $values['email_format'],
      'email_content' => [
        'from' => [
          'value' => $values['from'],
        ],
        'cc' => [
          'value' => $values['cc'] ?? '',
        ],
        'bcc' => [
          'value' => $values['bcc'] ?? '',
        ],
        'to' => [
          'value' => $values['to'] ?? '',
        ],
        'reply_to' => [
          'value' => $values['reply_to'] ?? '',
        ],
        'subject' => [
          'value' => $values['subject'] ?? '',
        ],
        'message_plain' => [
          'value' => $values['message_plain'] ?? '',
        ],
        'message_html' => [
          'value' => $values['message_html']['value'] ?? '',
          'format' => $values['message_html']['format'] ?? '',
        ],
        'attachments' => [
          'value' => $values['attachments'] ?? '',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity, array $context = []) {
    $configuration = $this->preparePluginSettings($entity);
    // Needed elements to send the email.
    $_operation = $context['operation'] ?? '';
    $_current_langcode = $this->languageManager->getCurrentLanguage()->getId();
    $_format = $configuration['settings']['email_format'];
    $_from = $configuration['settings']['email_content']['from']['processed'];
    $_to = $configuration['settings']['email_content']['to']['processed'];
    $_cc = $configuration['settings']['email_content']['cc']['processed'];
    $_bcc = $configuration['settings']['email_content']['bcc']['processed'];
    $_subject = $configuration['settings']['email_content']['subject']['processed'];
    $_reply_to = $configuration['settings']['email_content']['reply_to']['processed'];
    $_body = $configuration['settings']['email_content']['message_' . $_format]['processed'];
    $_attachments = $configuration['settings']['email_content']['attachments']['processed'];
    // We need to clean-up other values before passing to other hooks.
    // Also, passing context and entity for alter hook implementations.
    $_params = [
      '_data' => [
        'entity' => $entity,
        'context' => $context,
      ],
      'email_format' => $_format,
      'from' => $_from,
      'to' => $_to,
      'cc' => $_cc,
      'bcc' => $_bcc,
      'reply_to' => $_reply_to,
      'subject' => $_subject,
      'body' => $_body,
    ];
    if (!empty($_attachments)) {
      $_params['attachments'] = $_attachments;
    }
    $this->mailManager->mail(
      'auctioneer_handlers',
      'auctioneer_handler_' . $_format . '_email_' . $entity->getEntityTypeId() . (!empty($_operation) ? '_' . $_operation : ''),
      $_to,
      $_current_langcode,
      $_params,
      $_from
    );
  }

  /**
   * {@inheritdoc}
   */
  public function preparePluginSettings(ContentEntityInterface $entity) : array {
    $configuration = parent::preparePluginSettings($entity);
    if (!empty($_ns = $configuration['settings'] ?? [])) {
      $_replacements = [
        $entity->getEntityTypeId() => $entity,
      ];
      $_options = $this->prepareTokenOptions();
      foreach ($_ns['email_content'] as $key => $values) {
        $_ns['email_content'][$key]['processed'] = $this->replaceTokens($values['value'], $_replacements, $_options);
      }
      if ($_ns['email_format'] == 'html') {
        $message = [
          '#type' => 'processed_text',
          '#text' => $_ns['email_content']['message_html']['processed'],
          '#format' => $_ns['email_content']['message_html']['format'],
        ];
        // Processing as the selected text format.
        $_ns['email_content']['message_html']['processed'] = $this->renderer->renderPlain($message);
        // Also, content needs to be formatted as "Markup".
        $_ns['email_content']['message_html']['processed'] = Markup::create(
          $_ns['email_content']['message_html']['processed']
        );
      }
      $_attachments = !empty($_ns['email_content']['attachments']['processed']) ? explode(',', $_ns['email_content']['attachments']['processed']) : [];
      $_files = [];
      if (!empty($_attachments)) {
        $_uri = $_filename = NULL;
        foreach ($_attachments as $_attachment) {
          $_file = [];
          [$_uri, $_filename] = explode('|', $_attachment);
          if (file_exists(trim($_uri))) {
            $_file['filepath'] = trim($_uri);
            if (!empty(trim($_filename))) {
              $_file['filename'] = trim($_filename);
            }
            array_push($_files, $_file);
          }
        }
      }
      $_ns['email_content']['attachments']['processed'] = $_files;
      // Finally, setting-up processed settings.
      $configuration['settings'] = $_ns;
    }

    return $configuration;
  }

  /**
   * Validate a given list of emails.
   *
   * @param array $emails
   *   The emails list to validate.
   * @param bool $keep_tokens
   *   Boolean indicating if needed to bypass tokens.
   *
   * @return array
   *   The valid and invalid emails.
   */
  public function validateEmails(array $emails, $keep_tokens = FALSE) : array {
    $result = [
      'valid' => [],
      'invalid' => [],
    ];
    foreach ($emails as $email) {
      if (!is_string($email)) {
        array_push($result['invalid'], $email);
        continue;
      }
      if (preg_match('/^\[\S*\]/', trim($email))) {
        if ($keep_tokens) {
          array_push($result['valid'], $email);
        }
        continue;
      }
      if ($this->emailValidator->isValid(trim($email))) {
        array_push($result['valid'], $email);
      }
      else {
        array_push($result['invalid'], $email);
      }
    }

    return $result;
  }

}
