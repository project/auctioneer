<?php

namespace Drupal\auctioneer_handlers\Plugin\Auctioneer\Handler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\auctioneer_handlers\CommonHandler;
use Telegram\Bot\Api as TelegramBotApi;

/**
 * Provides the "TelegramChatMessage" handler plugin.
 *
 * @Handler(
 *   id = "auctioneer_handler_telegram_chat_message",
 *   label = "Telegram chat message",
 *   description = "Send a message into a Telegram conversation.",
 *   scope = "bid, auction"
 * )
 */
class TelegramChatMessage extends CommonHandler {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $configuration = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state, $entity);
    $form['bot_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bot token'),
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['telegram']['bot_token']['value'] ?? '',
      '#description' => $this->t('The bot token.'),
    ];
    $form['chat_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat ID'),
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['telegram']['chat_id']['value'] ?? '',
      '#description' => $this->t('The chat ID.'),
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
      '#default_value' => $configuration['settings']['telegram']['message']['value'] ?? '',
      '#description' => $this->t('The message to be sent to the chat. Telegram supports the usage of following tags: <em>@allowed_tags</em>', ['@allowed_tags' => 'b, i, a, br, pre, code']),
    ];
    $form['guide'] = [
      '#type' => 'container',
      'bot_token' => [
        '#type' => 'container',
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $this->t('To get existing bot token'),
        ],
        'content' => [
          '#theme' => 'item_list',
          '#items' => [
            $this->t('Message to <strong>@both</strong> and execute command <strong>@command</strong>.', ['@both' => '@BotFather', '@command' => '/token']),
            $this->t('Pick your existing bot.'),
            $this->t('Done. The previous command will display something like <em>@example</em>. Copy it and keep it safe.', ['@example' => '1234567890:FBE9_Pj6jOiOcQOPaYAm6a5HsnaL4EBApTeg']),
          ],
        ],
      ],
      'chat_id' => [
        '#type' => 'container',
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $this->t('To get chat ID'),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('There are several ways to get chat ID. This one will make use of the oficial Telegram API.'),
        ],
        'content' => [
          '#theme' => 'item_list',
          '#items' => [
            $this->t('Ensure your bot is added into the group or channel.'),
            $this->t('Ensure there is a recent activity into the channel or group. For example, sending a message.'),
            $this->t(
              'Go to the page <em>:bot_page</em>. Find into the displayed JSON the proper channel/group ID. For example: <em>:example</em>',
              [
                ':bot_page' => 'https://api.telegram.org/bot<BOT_ID>/getUpdates',
                ':example' => 'https://api.telegram.org/bot1234567890:FBE9_Pj6jOiOcQOPaYAm6a5HsnaL4EBApTeg/getUpdates',
              ]
            ),
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    if (!class_exists('\Telegram\Bot\Api')) {
      $form_state->setError(
        $form,
        $this->t(
          'To use this handler, you must to install the <em>Telegram Bot SDK</em>. See <a href=":page" target="_blank">the official page</a> for more information.',
          [
            ':page' => 'https://telegram-bot-sdk.com/',
          ]
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {
    $values = $form_state->getValues();
    $this->configuration = [
      'telegram' => [
        'bot_token' => [
          'value' => $values['bot_token'],
        ],
        'chat_id' => [
          'value' => $values['chat_id'],
        ],
        'message' => [
          'value' => $values['message'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity, array $context = []) {
    $configuration = $this->preparePluginSettings($entity);
    // Needed values to execute our handler.
    $_bot_token = $configuration['settings']['telegram']['bot_token']['processed'] ?? '';
    $_chat_id = $configuration['settings']['telegram']['chat_id']['processed'] ?? '';
    $_message = $configuration['settings']['telegram']['message']['processed'] ?? '';
    if ($_bot_token && $_chat_id && $_message) {
      try {
        $telegram = new TelegramBotApi($_bot_token);
        $telegram->sendMessage([
          'chat_id' => $_chat_id,
          'text' => $_message,
          'parse_mode' => 'html',
        ]);
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('auctioneer_handlers')->debug(
          'Error executing handler instance <em>%handler_instance</em> on entity type <em>%entity_type</em> with ID <em>%entity_id</em>: %error_message',
          [
            '%handler_instance' => $context['instance_id'],
            '%entity_type' => $entity->getEntityTypeId(),
            '%entity_id' => $entity->id(),
            '%error_message' => $e->getMessage(),
          ]
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preparePluginSettings(ContentEntityInterface $entity) : array {
    $configuration = parent::preparePluginSettings($entity);
    if (!empty($_ns = $configuration['settings'] ?? [])) {
      $_replacements = [
        $entity->getEntityTypeId() => $entity,
      ];
      $_options = $this->prepareTokenOptions();
      foreach ($_ns['telegram'] as $key => $values) {
        $_ns['telegram'][$key]['processed'] = $this->replaceTokens($values['value'], $_replacements, $_options);
      }
      // Finally, setting-up processed settings.
      $configuration['settings'] = $_ns;
    }

    return $configuration;
  }

}
