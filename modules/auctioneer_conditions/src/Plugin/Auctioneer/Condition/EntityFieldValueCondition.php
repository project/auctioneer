<?php

namespace Drupal\auctioneer_conditions\Plugin\Auctioneer\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\auctioneer\Condition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the "EntityFieldValueCondition" condition plugin.
 *
 * @Condition(
 *   id = "auctioneer_condition_entity_field_value",
 *   label = "Field value",
 *   description = "Validate an entity field value.",
 *   scope = "bid, auction"
 * )
 */
class EntityFieldValueCondition extends Condition {


  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []) {
    if (isset($context['config_entity']) && $context['config_entity'] instanceof ConfigEntityInterface) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions(
        $context['config_entity']->getEntityType()->getBundleOf(),
        $context['config_entity']->id()
      );
      $field_options = [];
      foreach ($field_definitions as $field_definition) {
        $field_options[$field_definition->getName()] = $field_definition->getLabel();
      }
      $form['field_name'] = [
        '#type' => 'select',
        '#title' => $this->t('Field to evaluate'),
        '#options' => $field_options,
        '#required' => TRUE,
        '#default_value' => $this->configuration['field_name'] ?? NULL,
      ];
      $form['operator'] = [
        '#type' => 'select',
        '#title' => $this->t('Operator'),
        '#options' => [
          'EMPTY' => $this->t('Empty'),
          'NOT_EMPTY' => $this->t('Not empty'),
          'IN' => $this->t('In'),
          'NOT_IN' => $this->t('Not in'),
          'BETWEEN' => $this->t('Between'),
          'NOT_BETWEEN' => $this->t('Not between'),
          'CONTAINS' => $this->t('Contains'),
          'NOT_CONTAINS' => $this->t('Does not contain'),
          '==' => $this->t('Equal'),
          '!=' => $this->t('Not equal'),
          '>' => $this->t('Greater than'),
          '>=' => $this->t('Greater or equal to'),
          '<' => $this->t('Lower than'),
          '<=' => $this->t('Lower or equal to'),
        ],
        '#required' => TRUE,
        '#default_value' => $this->configuration['operator'] ?? NULL,
      ];
      $form['comparison'] = [
        '#type' => 'container',
        '#tree' => TRUE,
        'comparison_value' => [
          '#type' => 'textfield',
          '#title' => $this->t('Comparison value'),
          '#default_value' => $this->configuration['value'] ?? NULL,
          '#states' => [
            'invisible' => [
              'select[name="plugin[settings][operator]"]' => [
                ['value' => 'EMPTY'],
                ['value' => 'NOT_EMPTY'],
              ],
            ],
          ],
        ],
        'comparison_guidelines' => [
          '#type' => 'container',
          '#tree' => FALSE,
          'multiple' => [
            '#type' => 'container',
            '#tree' => FALSE,
            'text' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $this->t('Multiple values may be added, separated by "@separator".', ['@separator' => '|']),
              '#attributes' => [
                'class' => ['form-item__description'],
              ],
            ],
            '#states' => [
              'visible' => [
                'select[name="plugin[settings][operator]"]' => [
                  ['value' => 'IN'],
                  ['value' => 'NOT_IN'],
                  ['value' => 'BETWEEN'],
                  ['value' => 'NOT_BETWEEN'],
                ],
              ],
            ],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []) {
    $values = $form_state->getValues();
    // Different validations, according to certain rules.
    $comparison_operator = $values['operator'];
    $comparison_value = $values['comparison']['comparison_value'] ?? '';
    if (!in_array($comparison_operator, ['EMPTY', 'NOT_EMPTY', '=', '!=']) && $comparison_value == '') {
      $form_state->setError($form['comparison']['comparison_value'], $this->t('Comparison value cannot be empty.'));
    }
    if (in_array($comparison_operator, ['>', '>=', '<', '<=']) && !is_numeric($comparison_value)) {
      $form_state->setError($form['comparison']['comparison_value'], $this->t('Comparison value must be of type numeric.'));
    }
    if (in_array($comparison_operator, ['BETWEEN', 'NOT_BETWEEN'])) {
      $fragments = explode('|', $comparison_value);
      if (count($fragments) != 2) {
        $form_state->setError($form['comparison']['comparison_value'], $this->t('Two values are required for this comparison.'));
      }
      if (!is_numeric($fragments[0]) || !is_numeric($fragments[1])) {
        $form_state->setError($form['comparison']['comparison_value'], $this->t('Values should be of type numeric.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []) {
    $values = $form_state->getValues();
    $this->configuration = [
      'field_name' => $values['field_name'],
      'operator' => $values['operator'],
      'value' => $values['comparison']['comparison_value'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(array $params = []) : bool {
    $valid = FALSE;
    if (isset($params['entity']) && $params['entity'] instanceof ContentEntityInterface) {
      $entity = $params['entity'];
      $field_name = $this->configuration['field_name'];
      if ($entity->hasField($field_name)) {
        $operator = $this->configuration['operator'];
        $value = $this->configuration['value'];
        $field_value = $entity->get($field_name)->getString();
        switch ($operator) {
          case 'EMPTY':
            $valid = empty($field_value);
            break;

          case 'NOT_EMPTY':
            $valid = !empty($field_value);
            break;

          case 'IN':
            $valid = in_array($field_value, explode('|', str_replace(' ', '', $value)));
            break;

          case 'NOT_IN':
            $valid = !in_array($field_value, explode('|', str_replace(' ', '', $value)));
            break;

          case 'BETWEEN':
            $min = $max = 0;
            if (is_numeric($field_value)) {
              [$min, $max] = explode('|', str_replace(' ', '', $value));
              $valid = ($field_value >= $min) && ($field_value <= $max);
            }
            break;

          case 'NOT_BETWEEN':
            $min = $max = 0;
            if (is_numeric($field_value)) {
              [$min, $max] = explode('|', str_replace(' ', '', $value));
              $valid = ($field_value < $min) || ($field_value > $max);
            }
            break;

          case 'CONTAINS':
            $valid = str_contains($field_value, $value);
            break;

          case 'NOT_CONTAINS':
            $valid = !str_contains($field_value, $value);
            break;

          case '==':
            $valid = $field_value == $value;
            break;

          case '!=':
            $valid = $field_value != $value;
            break;

          case '>':
            $valid = $field_value > $value;
            break;

          case '>=':
            $valid = $field_value >= $value;
            break;

          case '<':
            $valid = $field_value < $value;
            break;

          case '<=':
            $valid = $field_value <= $value;
            break;
        }
      }
    }

    return $valid;
  }

}
