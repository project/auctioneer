<?php

namespace Drupal\auctioneer_conditions\Plugin\Auctioneer\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\auctioneer\Condition;

/**
 * Provides the "CurrentUserRoles" condition plugin.
 *
 * This is an example about how condition plugins are not dependant on handlers.
 *
 * @Condition(
 *   id = "auctioneer_condition_current_user_roles",
 *   label = "Current user roles",
 *   description = "Check if current user has certain roles.",
 *   scope = "bid, auction"
 * )
 */
class CurrentUserRoles extends Condition {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []) {
    $configuration = $this->configuration;
    $roles = [];
    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
      $roles[$role->id()] = $role->label();
    }
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed user roles'),
      '#options' => $roles,
      '#required' => TRUE,
      '#default_value' => $configuration['roles'] ?? [],
    ];
    $form['validation'] = [
      '#type' => 'select',
      '#title' => $this->t('Validation'),
      '#options' => [
        'ALL' => $this->t('Contains all selected roles'),
        'ANY' => $this->t('Contains any of the selected roles'),
      ],
      '#required' => TRUE,
      '#default_value' => $configuration['validation'] ?? [],
    ];
    $form['reverse_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reverse validation'),
      '#default_value' => $configuration['reverse'] ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []) {
    $values = $form_state->getValues();
    $this->configuration = [
      'roles' => array_values(array_filter($values['roles'])),
      'validation' => $values['validation'],
      'reverse' => (bool) $values['reverse_validation'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(array $params = []) : bool {
    $configuration = $this->configuration;
    $user_roles = $this->account->getRoles();
    $valid = FALSE;
    $intersect = array_intersect($user_roles, $configuration['roles']);
    // Counts for intersected and configured roles.
    $_c0 = count($intersect);
    $_c1 = count($configuration['roles']);
    switch ($configuration['validation']) {
      case 'ALL':
        $valid = $configuration['reverse'] ? $_c0 == 0 : $_c0 == $_c1;
        break;

      case 'ANY':
        $valid = $configuration['reverse'] ? $_c0 <= $_c1 : $_c0 > 0;
        break;
    }

    return $valid;
  }

}
