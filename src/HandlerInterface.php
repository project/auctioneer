<?php

namespace Drupal\auctioneer;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Defines the interface for handler plugins.
 */
interface HandlerInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Set label for this handler instance.
   *
   * @param string $label
   *   The label to be used.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setLabel(string $label);

  /**
   * Get specified label on this handler.
   *
   * @return string
   *   The handler label.
   */
  public function getLabel();

  /**
   * Set handler description.
   *
   * @param string $description
   *   The handler description.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setDescription(string $description);

  /**
   * Get handler description.
   *
   * @return string
   *   The handler description.
   */
  public function getDescription();

  /**
   * Set handler status.
   *
   * @param bool $status
   *   TRUE or FALSE to enable or disable this handler.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setStatus(bool $status);

  /**
   * Get handler status.
   *
   * @return bool
   *   The handler status.
   */
  public function getStatus();

  /**
   * Set handler weight.
   *
   * @param int $weight
   *   The weight for this handler.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setWeight(int $weight);

  /**
   * Get handler weight.
   *
   * @return int
   *   The handler weight.
   */
  public function getWeight();

  /**
   * Get triggering event names.
   *
   * @return array
   *   The triggering events.
   */
  public function getEvents();

  /**
   * Set triggering event names.
   *
   * @param array $events
   *   The event names when this handler should be executed.
   *   For example ["create", "update", "delete"].
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setEvents(array $events);

  /**
   * Constructs the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity type where this handler will be attached on.
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL);

  /**
   * Validate the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity type where this handler will be attached on.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL);

  /**
   * Submit the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity type where this handler will be attached on.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL);

  /**
   * Execute this handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity where this handler will act on.
   * @param array $context
   *   Miscellaneous information passed to the plugin instance.
   */
  public function execute(ContentEntityInterface $entity, array $context = []);

  /**
   * Set handler configuration.
   *
   * @param array $configuration
   *   The handler array settings.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The auction handler instance.
   */
  public function setConfiguration(array $configuration);

  /**
   * Get handler configuration.
   *
   * @return array
   *   The handler configuration.
   */
  public function getConfiguration();

  /**
   * Provide a default configuration.
   *
   * @return array
   *   The handler default configuration.
   */
  public function defaultConfiguration();

  /**
   * Calculate dependencies.
   *
   * @return array
   *   The handler calculated dependencies (not implemented yet).
   */
  public function calculateDependencies();

  /**
   * Helper method to get plugin manager.
   *
   * @return mixed|object|null
   *   The conditions plugin manager.
   */
  public function getConditionsPluginManager();

  /**
   * Get conditions configuration.
   */
  public function getConditionsConfiguration();

  /**
   * Get loaded conditions collection.
   */
  public function getConditions();

  /**
   * Get conditions cardinality, AKA "operator".
   */
  public function getConditionsCardinality();

  /**
   * Set conditions cardinality, AKA "operator".
   *
   * @param string $cardiality
   *   The cardinality/operator.
   */
  public function setConditionsCardinality(string $cardiality);

  /**
   * Wrapper method to add a new condition.
   *
   * @param \Drupal\auctioneer\ConditionInterface $condition
   *   The condition to be added.
   */
  public function addCondition(ConditionInterface $condition);

  /**
   * Wrapper method to edit a condition.
   *
   * @param string $instance_id
   *   The condition instance ID.
   * @param \Drupal\auctioneer\ConditionInterface $condition
   *   The condition to be edited.
   */
  public function editCondition(string $instance_id, ConditionInterface $condition);

  /**
   * Wrapper method to remove a condition.
   *
   * @param string $instance_id
   *   The condition instance ID.
   */
  public function removeCondition(string $instance_id);

  /**
   * Get single condition instance.
   *
   * @param string $instance_id
   *   The condition instance ID.
   *
   * @return \Drupal\auctioneer\ConditionInterface
   *   The condition instance.
   */
  public function getCondition(string $instance_id);

  /**
   * Check conditions attached to the current handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity being affected on this handler execution.
   *
   * @return bool
   *   TRUE or FALSE depending if conditions are valid or not.
   */
  public function validateConditions(ContentEntityInterface $entity) : bool;

}
