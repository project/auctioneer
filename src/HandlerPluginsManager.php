<?php

namespace Drupal\auctioneer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Manages auctioneer handler plugins.
 *
 * @see plugin_api
 */
class HandlerPluginsManager extends DefaultPluginManager {

  /**
   * Constructs a new HandlerPluginsManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Auctioneer/Handler',
      $namespaces,
      $module_handler,
      'Drupal\auctioneer\HandlerInterface',
      'Drupal\auctioneer\Annotation\Handler'
    );
    $this->alterInfo('auctioneer_handler_info');
    $this->setCacheBackend($cache_backend, 'auctioneer_handler_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    foreach (['id', 'label', 'description', 'scope'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The handler plugin "%s" must define the "%s" property', $plugin_id, $required_property));
      }
    }
    if (!empty($definition['scope'])) {
      $scopes = explode(',', str_replace(' ', '', $definition['scope']));
      $diff = array_diff($scopes, ['bid', 'auction']);
      if (count($diff) > 0) {
        throw new PluginException(sprintf('The handler plugin "%s" contains wrong scope(s) "%s" defined. Allowed values are "%s"', $plugin_id, implode(', ', $diff), 'bid, auction'));
      }
    }
  }

}
