<?php

namespace Drupal\auctioneer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\Exception\PluginException;

/**
 * Manages auctioneer condition plugins.
 *
 * @see plugin_api
 */
class ConditionPluginsManager extends DefaultPluginManager {

  /**
   * Constructs a new ConditionPluginsManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Auctioneer/Condition',
      $namespaces,
      $module_handler,
      'Drupal\auctioneer\ConditionInterface',
      'Drupal\auctioneer\Annotation\Condition'
    );
    $this->alterInfo('auctioneer_condition_info');
    $this->setCacheBackend($cache_backend, 'auctioneer_condition_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    foreach (['id', 'label', 'description', 'scope'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The condition plugin "%s" must define the "%s" property', $plugin_id, $required_property));
      }
    }
  }

}
