<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base form to delete handlers.
 */
abstract class HandlerDeleteBaseForm extends ConfirmFormBase {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The handler machine name to delete.
   *
   * @var string
   */
  protected $handler;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL, $handler = NULL) {
    $this->entity = $entity;
    $this->handler = $handler;
    $form = parent::buildForm($form, $form_state);
    if ($this->entity instanceof ConfigEntityInterface && $handler) {
      $configuration = $entity->getHandler($handler);
      if (empty($configuration)) {
        $form['warning'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Invalid or inexistent handler ID.'),
        ];
        $form['actions']['#access'] = FALSE;
        $form['description']['#access'] = FALSE;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to delete the handler <em>%handler</em>?', ['%handler' => $this->handler]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $this->entity->removeHandler($this->handler);
      $this->entity->save();
      $this->messenger()->addMessage('Handler was deleted.');
      $form_state->setRedirectUrl($this->entity->toUrl('handlers'));
    }
  }

  /**
   * Helper function to determine the nature of this form.
   *
   * @return string
   *   The "delete" handler operation.
   */
  protected function handlerOperation() {
    return 'delete';
  }

}
