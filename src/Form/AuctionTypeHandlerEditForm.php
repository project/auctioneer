<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to edit auction handlers.
 */
class AuctionTypeHandlerEditForm extends HandlerConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_auction_type_handler_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function handlerOperation() {
    return 'update';
  }

}
