<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the auction add/edit form.
 */
class AuctionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
    $auction = $this->entity;
    $form['#tree'] = TRUE;
    $form['#theme'] = ['auctioneer_auction_form'];
    $form['#attached']['library'][] = 'auctioneer/auction.form.style';
    $form['status']['#group'] = 'footer';
    $form['advanced'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 100,
    ];
    $form['meta'] = [
      '#attributes' => [
        'class' => [
          'entity-meta__header',
        ],
      ],
      '#type' => 'container',
      '#group' => 'advanced',
      '#weight' => -100,
      'published' => [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $auction->isNew() ? $this->t('New auction') : ($auction->isPublished() ? $this->t('Published') : $this->t('Not published')),
        '#attributes' => [
          'class' => ['entity-meta__title'],
        ],
      ],
    ];
    $form['path_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('URL path settings'),
      '#open' => !empty($form['path']['widget'][0]['alias']['#default_value']),
      '#group' => 'advanced',
      '#access' => !empty($form['path']['#access']) && $auction->get('path')->access('edit'),
      '#attributes' => [
        'class' => ['path-form'],
      ],
      '#attached' => [
        'library' => ['path/drupal.path'],
      ],
      '#weight' => 90,
    ];
    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Authoring information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['auction-form-author'],
      ],
      '#weight' => 80,
      '#optional' => TRUE,
    ];
    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }
    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }
    if (isset($form['path'])) {
      $form['path']['#group'] = 'path_settings';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
    $auction = $this->getEntity();
    $auction->save();
    $this->messenger()->addMessage($this->t('The auction %label has been successfully saved.', ['%label' => $auction->label()]));
    $form_state->setRedirect('entity.auction.canonical', ['auction' => $auction->id()]);
  }

}
