<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to add bid handlers.
 */
class BidTypeHandlerConditionAddForm extends HandlerConditionConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_bid_type_handler_condition_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function conditionOperation() {
    return 'create';
  }

}
