<?php

namespace Drupal\auctioneer\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form to manage handlers.
 */
abstract class HandlerConfigurationBaseForm extends FormBase {

  /**
   * The handler plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $handlerPluginManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The handler instance.
   *
   * @var \Drupal\Auctioneer\HandlerInterface
   */
  protected $handler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->handlerPluginManager = $container->get('plugin.manager.auctioneer.handler');
    $instance->moduleHandler = $container->get('module_handler');

    return $instance;
  }

  /**
   * Current scope for this entity type.
   *
   * @return string
   *   The entity type id.
   */
  public function currentScope() : string {
    $scope = '';
    if ($this->entity instanceof ConfigEntityInterface) {
      $scope = $this->entity->getEntityType()->getBundleOf();
    }

    return $scope;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL, $handler = NULL) {
    if ($entity instanceof ConfigEntityInterface) {
      $this->entity = $entity;
      $handler_operation = $this->handlerOperation();
      try {
        $this->handler = $this->prepareHandlerInstance(
          $handler,
          $handler_operation == 'create' ? 'new' : 'existing',
        );
      }
      catch (PluginNotFoundException $e) {
        throw new NotFoundHttpException("Handler ID not found: '$handler'.");
      }
      $handler_plugin_definition = $this->handler->getPluginDefinition();
      $entity_type_id = $this->currentScope();
      if (strpos($handler_plugin_definition['scope'], $entity_type_id) === FALSE) {
        throw new InvalidPluginDefinitionException($handler, 'Scope for requested plugin is invalid.');
      }
      $form['handler'] = [
        '#type' => 'value',
        '#value' => $handler,
      ];
      $form['info'] = [
        '#type' => 'container',
        '#tree' => TRUE,
        'plugin_name' => [
          '#type' => 'value',
          '#value' => $handler_plugin_definition['id'],
        ],
        'plugin_label' => [
          '#type' => 'html_tag',
          '#tag' => 'h2',
          '#value' => $handler_plugin_definition['label'] ?? '',
        ],
        'plugin_description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $handler_plugin_definition['description'] ?? '',
        ],
      ];
      $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#required' => TRUE,
      ];
      $form['description'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description'),
      ];
      $form['plugin'] = [
        '#type' => 'details',
        '#title' => $this->t('Plugin settings'),
        '#tree' => FALSE,
        '#open' => TRUE,
      ];
      $form['plugin']['settings'] = [
        '#tree' => TRUE,
        '#parents' => ['plugin', 'settings'],
      ];
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $form['plugin']['settings'] = $this->handler->buildConfigurationForm($form['plugin']['settings'], $subform_state, $this->entity);
      // Disable client-side HTML5 validation which is having issues with hidden
      // element validation.
      // @see http://stackoverflow.com/questions/22148080/an-invalid-form-control-with-name-is-not-focusable
      if (isset($form['plugin']['settings']['#attributes']['novalidate'])) {
        $form['#attributes']['novalidate'] = 'novalidate';
      }
      $form['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#description' => $this->t('Enable or disable this handler.'),
      ];
      $form['events'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Triggering events'),
        '#required' => TRUE,
        '#options' => [
          'create' => $this->t('When creating a new entity'),
          'update' => $this->t('When updating an existing entity'),
          'delete' => $this->t('When deleting an existing entity'),
        ],
        'create' => [
          '#description' => $this->t('The handler will be fired only when a new content entity is saved.'),
        ],
        'update' => [
          '#description' => $this->t('The handler will be fired only when an existing entity is updated.'),
        ],
        'delete' => [
          '#description' => $this->t('The handler will be fired only when an entity is deleted.'),
        ],
      ];
      if ($entity_type_id == 'bid') {
        // Hammer event.
        $form['events']['#options']['hammer'] = $this->t('Hammer bid');
        $form['events']['hammer']['#description'] = $this->t('When a bid is marked as winner');
        // Unhammer event.
        $form['events']['#options']['unhammer'] = $this->t('Unhammer bid');
        $form['events']['unhammer']['#description'] = $this->t('When a bid is unmarked as winner');
      }
      $_custom_events = $this->moduleHandler->invokeAll('auctioneer_handler_event_info');
      foreach ($_custom_events as $_event_name => $_event_properties) {
        if (!isset($form['events']['#options'][$_event_name]) && in_array($entity_type_id, $_event_properties['scope'])) {
          $form['events']['#options'][$_event_name] = $_event_properties['label'];
          $form['events'][$_event_name]['#description'] = $_event_properties['description'];
        }
      }
      $form['actions'] = [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
        ],
        'return' => [
          '#type' => 'link',
          '#title' => $this->t('Return to handlers list'),
          '#url' => $this->entity->toUrl('handlers'),
          '#attributes' => [
            'class' => ['button'],
          ],
        ],
      ];
      // Setting-up default values if this is an edit form.
      if ($handler_operation == 'update') {
        $configuration = $this->handler->getConfiguration();
        // Setting-up default values.
        $form['label']['#default_value'] = $configuration['label'] ?? '';
        $form['description']['#default_value'] = $configuration['description'] ?? '';
        $form['status']['#default_value'] = $configuration['status'] ?? FALSE;
        $form['events']['#default_value'] = $configuration['events'] ?? [];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $this->handler->validateConfigurationForm($form['plugin']['settings'], $subform_state, $this->entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $this->handler->submitConfigurationForm($form['plugin']['settings'], $subform_state, $this->entity);
      $values = $form_state->getValues();
      // Now, setting common values for add/edit.
      $this->handler->setLabel($values['label']);
      $this->handler->setDescription($values['description']);
      $this->handler->setStatus((bool) $values['status']);
      $this->handler->setEvents(array_values(array_filter($values['events'])));
      // We need to determine if it's a "create" or "update" form.
      $operation = $this->handlerOperation();
      switch ($operation) {
        case 'create':
          $this->entity->addHandler($this->handler->getConfiguration());
          $this->entity->save();
          $this->messenger()->addStatus($this->t('The handler was successfully added.'));
          break;

        case 'update':
          $this->entity->editHandler($values['handler'], $this->handler->getConfiguration());
          $this->entity->save();
          $this->messenger()->addStatus($this->t('The handler was successfully updated.'));
          break;
      }
      $form_state->setRedirectUrl($this->entity->toUrl('handlers'));
    }
  }

  /**
   * Prepare a handler instance. Used to add or edit.
   *
   * @param string $handler_id
   *   The handler to be initialized.
   * @param string $op
   *   Use "new" if new instance, or "existing" for an existing one.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The handler instance.
   */
  protected function prepareHandlerInstance(string $handler_id, string $op) {
    $handler = NULL;
    switch ($op) {
      case 'new':
        $handler = $this->handlerPluginManager->createInstance($handler_id);
        $existing_handlers = $this->entity->getHandlers();
        $weight = 0;
        foreach ($existing_handlers as $existing_handler) {
          if ($weight <= $existing_handler->getWeight()) {
            $weight = $existing_handler->getWeight() + 1;
          }
        }
        $handler->setWeight($weight);
        break;

      case 'existing':
        $handler = $this->entity->getHandler($handler_id);
        break;
    }

    return $handler;
  }

  /**
   * Helper function to determine the nature of this form.
   *
   * @return string
   *   "create" or "update" depending on used form.
   */
  protected function handlerOperation() {
    return '';
  }

}
