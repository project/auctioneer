<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to edit bid handlers.
 */
class BidTypeHandlerEditForm extends HandlerConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_bid_type_handler_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function handlerOperation() {
    return 'update';
  }

}
