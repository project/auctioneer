<?php

namespace Drupal\auctioneer\Form;

/**
 * Form to manage handler conditions.
 */
class AuctionTypeHandlerConditionsCollectionForm extends HandlerConditionsCollectionBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_auction_type_handler_conditions_collection_form';
  }

}
