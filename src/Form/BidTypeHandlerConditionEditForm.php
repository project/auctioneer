<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to edit bid handler conditions.
 */
class BidTypeHandlerConditionEditForm extends HandlerConditionConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_bid_type_handler_condition_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function conditionOperation() {
    return 'update';
  }

}
