<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\auctioneer\AuctionTypeManagerInterface;

/**
 * Auction type entity class.
 */
class AuctionTypeEntityForm extends BundleEntityFormBase {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs the "AuctionTypeEntityForm" form.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager, MessengerInterface $messenger) {
    $this->auctionTypeManager = $auction_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('auctioneer.auction_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity_type = $this->entity;
    // Our form with custom properties/fields.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('Label for the %content_entity_id entity type (bundle).', ['%content_entity_id' => $entity_type->getEntityType()->getBundleOf()]),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\auctioneer\Entity\AuctionType::load',
      ],
      '#disabled' => !$entity_type->isNew(),
    ];
    $form['bid_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Bid type'),
      '#default_value' => $entity_type->getBidTypeName(),
      '#options' => $this->auctionTypeManager->getAvailableBidTypeNames(),
      '#required' => TRUE,
      '#disabled' => !$entity_type->isNew(),
      '#description' => $this->t('Select bid type to use on this Auction type.'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Auction type description.'),
      '#default_value' => $entity_type->getDescription(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $bid_types = $this->auctionTypeManager->getAvailableBidTypeNames(TRUE);
    if (empty($bid_types)) {
      return [
        'warning' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t(
            'You have to add at least one bid type in order to be able to add a new auction type. <a href="@add_bid_form">Add new bid type</a>.',
            [
              '@add_bid_form' => Url::fromRoute(
                'entity.bid_type.add_form',
                [],
                [
                  'query' => [
                    'destination' => Url::fromRoute('entity.auction_type.add_form')->toString(),
                  ],
                ]
              )->toString(),
            ]
          ),
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity_type = $this->entity;
    $status = $entity_type->save();
    if ($status == SAVED_NEW) {
      $bid_type_id = $form_state->getValue('bid_type');
      $entity_type = $this->auctionTypeManager->initializeAuctionTypeBaseConfigurations($bid_type_id, $entity_type);
      // Finally, just a typical notification.
      $this->messenger->addMessage($this->t('Created the %label %content_entity_id entity type.', [
        '%label' => $entity_type->label(),
        '%content_entity_id' => $entity_type->getEntityType()->getBundleOf(),
      ]));
    }
    else {
      $this->messenger->addMessage($this->t('Saved the %label %content_entity_id entity type.', [
        '%label' => $entity_type->label(),
        '%content_entity_id' => $entity_type->getEntityType()->getBundleOf(),
      ]));
    }
    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
  }

}
