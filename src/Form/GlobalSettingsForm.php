<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Auctioneer global settings form.
 */
class GlobalSettingsForm extends ConfigFormBase {

  /**
   * Config settings name.
   *
   * @var string
   */
  const SETTINGS = 'auctioneer.settings.global';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_global_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->config(static::SETTINGS);
    $form['token'] = [
      '#type' => 'details',
      '#title' => $this->t('Token integration'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['token']['recursion_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Recursion limit'),
      '#default_value' => $configuration->get('token')['recursion_limit'] ?? 3,
      '#description' => $this->t('Default recursion limit when using token replacements. It is advised to keep low values to avoid memmory issues.'),
      '#required' => TRUE,
    ];
    $form['token']['clear'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Clear tokens'),
      '#default_value' => $configuration->get('token')['clear'] ?? TRUE,
      '#description' => $this->t('Clear empty tokens.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config(static::SETTINGS)
      ->set('token', [
        'recursion_limit' => intval($values['token']['recursion_limit']),
        'clear' => (bool) $values['token']['clear'],
      ])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
