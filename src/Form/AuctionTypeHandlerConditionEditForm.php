<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to edit auction type handler conditions.
 */
class AuctionTypeHandlerConditionEditForm extends HandlerConditionConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_auction_type_handler_condition_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function conditionOperation() {
    return 'update';
  }

}
