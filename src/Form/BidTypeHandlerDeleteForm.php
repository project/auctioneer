<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Url;

/**
 * Provides a bid type form to delete handlers.
 */
class BidTypeHandlerDeleteForm extends HandlerDeleteBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'auctioneer_bid_type_handler_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.bid_type.handlers', ['bid_type' => $this->entity->id()]);
  }

}
