<?php

namespace Drupal\auctioneer\Form;

/**
 * Form to manage auction type handlers.
 */
class AuctionTypeHandlersCollectionForm extends HandlersCollectionBaseForm {}
