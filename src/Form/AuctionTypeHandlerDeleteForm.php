<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Url;

/**
 * Provides an auction type form to delete handlers.
 */
class AuctionTypeHandlerDeleteForm extends HandlerDeleteBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'auctioneer_auction_type_handler_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.auction_type.handlers', ['auction_type' => $this->entity->id()]);
  }

}
