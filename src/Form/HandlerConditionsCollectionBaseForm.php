<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Base form to manage handler conditions.
 */
abstract class HandlerConditionsCollectionBaseForm extends FormBase {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL, $handler = NULL) {
    $user_input = $form_state->getUserInput();
    if ($entity instanceof ConfigEntityInterface) {
      $this->entity = $entity;
      try {
        $this->handler = $this->entity->getHandler($handler);
      }
      catch (PluginNotFoundException $e) {
        throw new NotFoundHttpException("Handler ID not found: '$handler'.");
      }
      $entity_type_id = $this->entity->getEntityTypeId();
      $rows = [];
      foreach ($this->handler->getConditions() as $key => $condition) {
        $configuration = $condition->getConfiguration();
        $row = [
          '#attributes' => [
            'class' => ['draggable'],
            'data-condition-key' => $key,
          ],
          '#weight' => (isset($user_input['handlers']) && isset($user_input['handlers'][$key])) ? $user_input['handlers'][$key]['weight'] : NULL,
          'label' => [
            'data' => ['#markup' => $configuration['label']],
          ],
          'description' => [
            'data' => ['#markup' => $configuration['description']],
          ],
          'plugin' => [
            'data' => ['#markup' => $configuration['id']],
          ],
          'weight' => [
            '#type' => 'weight',
            '#title' => $this->t('Weight for @title', ['@title' => '-']),
            '#title_display' => 'invisible',
            '#default_value' => $configuration['weight'],
            '#attributes' => [
              'class' => ['condition-order-weight'],
            ],
          ],
          'operations' => [
            '#type' => 'operations',
            '#links' => [
              'edit' => [
                'title' => $this->t('Edit'),
                'url' => Url::fromRoute(
                  "entity.{$entity_type_id}.handler.condition.edit_form",
                  [
                    'entity' => $this->entity->id(),
                    'handler' => $handler,
                    'condition' => $key,
                  ]
                ),
              ],
              'delete' => [
                'title' => $this->t('Delete'),
                'url' => Url::fromRoute(
                  "entity.{$entity_type_id}.handler.condition.delete_form",
                  [
                    'entity' => $this->entity->id(),
                    'handler' => $handler,
                    'condition' => $key,
                  ]
                ),
              ],
            ],
          ],
        ];
        $rows[$key] = $row;
      }
      $form['handler'] = [
        '#type' => 'container',
        '#tree' => TRUE,
        'label' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $this->handler->getLabel(),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->handler->getDescription(),
        ],
      ];
      $form['conditions'] = [
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('Label')],
          ['data' => $this->t('Description')],
          ['data' => $this->t('Plugin name')],
          ['data' => $this->t('Weight'), 'class' => ['tabledrag-hide']],
          ['data' => $this->t('Operations')],
        ],
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'condition-order-weight',
          ],
        ],
        '#attributes' => [
          'id' => 'conditions',
          'class' => ['conditions-table'],
        ],
        '#empty' => $this->t('There are currently no conditions defined for this handler.'),
      ] + $rows;
      $form['actions'] = [
        '#type' => 'actions',
      ];
      if (!empty($rows)) {
        $form['cardinality'] = [
          '#type' => 'select',
          '#title' => $this->t('Cardinality'),
          '#options' => [
            'AND' => $this->t('All conditions must pass'),
            'OR' => $this->t('Any condition must pass'),
          ],
          '#default_value' => $this->handler->getConditionsCardinality(),
        ];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save conditions'),
        ];
      }
      $form['actions']['return'] = [
        '#type' => 'link',
        '#title' => $this->t('Return to handlers list'),
        '#url' => $this->entity->toUrl('handlers'),
        '#attributes' => [
          'class' => ['button'],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values['conditions']) && !empty($values['cardinality'])) {
      foreach ($values['conditions'] as $key => $data) {
        if ($this->handler->getConditions()->has($key)) {
          $condition = $this->handler->getCondition($key);
          $condition->setWeight($data['weight']);
          $this->handler->editCondition($key, $condition);
        }
      }
      $this->handler->setConditionsCardinality($values['cardinality']);
      $this->entity->save();
      $this->messenger()->addStatus($this->t('Conditions were saved successfully.'));
    }
  }

}
