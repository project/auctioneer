<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a base form to delete handler conditions.
 */
abstract class HandlerConditionDeleteBaseForm extends ConfirmFormBase {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The handler machine name.
   *
   * @var string
   */
  protected $handler;

  /**
   * The condition machine name.
   *
   * @var string
   */
  protected $condition;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL, $handler = NULL, $condition = NULL) {
    $this->entity = $entity;
    $this->handler = $handler;
    $this->condition = $condition;
    $form = parent::buildForm($form, $form_state);
    if ($this->entity instanceof ConfigEntityInterface && $handler && $condition) {
      $configuration = $entity->getHandler($handler)->getCondition($condition);
      if (empty($configuration)) {
        $form['warning'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Invalid or inexistent condition ID.'),
        ];
        $form['actions']['#access'] = FALSE;
        $form['description']['#access'] = FALSE;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you really want to delete the handler condition <em>%condition</em>?', ['%condition' => $this->condition]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $this->entity->getHandler($this->handler)->removeCondition($this->condition);
      $this->entity->save();
      $entity_type_id = $this->entity->getEntityTypeId();
      $this->messenger()->addMessage('Condition was deleted.');
      $form_state->setRedirectUrl(
        Url::fromRoute(
          "entity.{$entity_type_id}.handler.conditions",
          [
            'entity' => $this->entity->id(),
            'handler' => $this->handler,
          ]
        )
      );
    }
  }

  /**
   * Helper function to determine the nature of this form.
   *
   * @return string
   *   The "delete" condition operation.
   */
  protected function handlerOperation() {
    return 'delete';
  }

}
