<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Base form to manage handler conditions.
 */
abstract class HandlersCollectionBaseForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $entity_type_id = $this->entity->getEntityTypeId();
    $rows = [];
    foreach ($this->entity->getHandlers() as $key => $handler) {
      $configuration = $handler->getConfiguration();
      $row = [];
      $row['#attributes']['class'][] = 'draggable';
      $row['#attributes']['data-handler-key'] = $key;
      $row['#weight'] = (isset($user_input['handlers']) && isset($user_input['handlers'][$key])) ? $user_input['handlers'][$key]['weight'] : NULL;
      $row['label'] = [
        'data' => ['#markup' => $configuration['label']],
      ];
      $row['description'] = [
        'data' => ['#markup' => $configuration['description']],
      ];
      $row['status'] = [
        'data' => ['#markup' => $configuration['status'] ? $this->t('Enabled') : $this->t('Disabled')],
      ];
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => '-']),
        '#title_display' => 'invisible',
        '#default_value' => $configuration['weight'],
        '#attributes' => [
          'class' => ['handler-order-weight'],
        ],
      ];
      $row['events'] = [
        'data' => [
          '#theme' => 'item_list',
          '#items' => $configuration['events'] ?? [],
        ],
      ];
      $row['plugin'] = [
        'data' => ['#markup' => $configuration['id']],
      ];
      $row['operations'] = [
        '#type' => 'operations',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute(
              "entity.{$entity_type_id}.handler.edit_form",
              [
                'entity' => $this->entity->id(),
                'handler' => $key,
              ]
            ),
          ],
          'conditions' => [
            'title' => $this->t('Conditions'),
            'url' => Url::fromRoute(
              "entity.{$entity_type_id}.handler.conditions",
              [
                'entity' => $this->entity->id(),
                'handler' => $key,
              ]
            ),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute(
              "entity.{$entity_type_id}.handler.delete_form",
              [
                'entity' => $this->entity->id(),
                'handler' => $key,
              ]
            ),
          ],
        ],
      ];
      $rows[$key] = $row;
    }
    $form['handlers'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Label')],
        ['data' => $this->t('Description')],
        ['data' => $this->t('Status')],
        ['data' => $this->t('Weight'), 'class' => ['tabledrag-hide']],
        ['data' => $this->t('Event keys')],
        ['data' => $this->t('Plugin name')],
        ['data' => $this->t('Operations')],
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'handler-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'handlers',
        'class' => ['handlers-table'],
      ],
      '#empty' => $this->t('There are currently no handlers defined for this entity type.'),
    ] + $rows;

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $form = parent::actionsElement($form, $form_state);
    if ($this->entity->getHandlers()->count()) {
      $form['submit']['#value'] = $this->t('Save handlers');
    }
    else {
      unset($form['submit']);
    }
    unset($form['delete']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->isValueEmpty('handlers')) {
      $this->updateHandlerWeights($form_state->getValue('handlers'));
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('The handlers for <em>%label</em> were saved.', ['%label' => $this->getEntity()->label()]));
  }

  /**
   * Updates entity handler weights.
   *
   * @param array $handlers
   *   Associative array with handlers having handler ids as keys and array
   *   with handler data as values.
   */
  protected function updateHandlerWeights(array $handlers) {
    foreach ($handlers as $key => $data) {
      if ($this->entity->getHandlers()->has($key)) {
        $this->entity->getHandler($key)->setWeight($data['weight']);
      }
    }
  }

}
