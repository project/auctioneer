<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to add auction handler conditions.
 */
class AuctionTypeHandlerConditionAddForm extends HandlerConditionConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_auction_type_handler_condition_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function conditionOperation() {
    return 'create';
  }

}
