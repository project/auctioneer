<?php

namespace Drupal\auctioneer\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form to manage handler conditions.
 */
abstract class HandlerConditionConfigurationBaseForm extends FormBase {

  /**
   * The handler plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $handlerPluginManager;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $conditionPluginManager;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityInterface
   */
  protected $entity;

  /**
   * The handler instance.
   *
   * @var \Drupal\Auctioneer\HandlerInterface
   */
  protected $handler;

  /**
   * The condition instance.
   *
   * @var \Drupal\Auctioneer\ConditionInterface
   */
  protected $condition;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->handlerPluginManager = $container->get('plugin.manager.auctioneer.handler');
    $instance->conditionPluginManager = $container->get('plugin.manager.auctioneer.condition');

    return $instance;
  }

  /**
   * Current scope for this entity type.
   *
   * @return string
   *   The entity type id.
   */
  public function currentScope() : string {
    $scope = '';
    if ($this->entity instanceof ConfigEntityInterface) {
      $scope = $this->entity->getEntityType()->getBundleOf();
    }

    return $scope;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL, $handler = NULL, $condition = NULL) {
    if ($entity instanceof ConfigEntityInterface) {
      $this->entity = $entity;
      try {
        $this->handler = $this->entity->getHandler($handler);
      }
      catch (PluginNotFoundException $e) {
        throw new NotFoundHttpException("Handler ID not valid: '$handler'.");
      }
      $entity_type_id = $this->entity->getEntityTypeId();
      $condition_operation = $this->conditionOperation();
      try {
        $this->condition = $this->prepareConditionInstance(
          $condition,
          $condition_operation == 'create' ? 'new' : 'existing',
        );
      }
      catch (PluginNotFoundException $e) {
        throw new NotFoundHttpException("Condition ID not valid: '$condition'.");
      }
      $condition_plugin_definition = $this->condition->getPluginDefinition();
      $scope = $this->currentScope();
      if (strpos($condition_plugin_definition['scope'], $scope) === FALSE) {
        throw new InvalidPluginDefinitionException($handler, 'Scope for requested plugin is invalid.');
      }
      $form['handler'] = [
        '#type' => 'value',
        '#value' => $handler,
      ];
      $form['condition'] = [
        '#type' => 'value',
        '#value' => $condition,
      ];
      $form['info'] = [
        '#type' => 'container',
        '#tree' => TRUE,
        'plugin_name' => [
          '#type' => 'value',
          '#value' => $condition_plugin_definition['id'],
        ],
        'plugin_label' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $condition_plugin_definition['label'] ?? '',
        ],
        'plugin_description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $condition_plugin_definition['description'] ?? '',
        ],
      ];
      $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#required' => TRUE,
      ];
      $form['description'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description'),
      ];
      $form['plugin'] = [
        '#type' => 'details',
        '#title' => $this->t('Plugin settings'),
        '#tree' => FALSE,
        '#open' => TRUE,
      ];
      $form['plugin']['settings'] = [
        '#tree' => TRUE,
        '#parents' => ['plugin', 'settings'],
      ];
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $form['plugin']['settings'] = $this->condition->buildConfigurationForm($form['plugin']['settings'], $subform_state, $this->provideContext());
      // Disable client-side HTML5 validation which is having issues with hidden
      // element validation.
      // @see http://stackoverflow.com/questions/22148080/an-invalid-form-control-with-name-is-not-focusable
      if (isset($form['plugin']['settings']['#attributes']['novalidate'])) {
        $form['#attributes']['novalidate'] = 'novalidate';
      }
      $form['actions'] = [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
        ],
        'return' => [
          '#type' => 'link',
          '#title' => $this->t('Return to conditions list'),
          '#url' => Url::fromRoute(
            "entity.{$entity_type_id}.handler.conditions",
            [
              'entity' => $this->entity->id(),
              'handler' => $handler,
            ]
          ),
          '#attributes' => [
            'class' => ['button'],
          ],
        ],
      ];
      // Setting-up default values if this is an edit form.
      if ($condition_operation == 'update') {
        $configuration = $this->condition->getConfiguration();
        // Setting-up default values.
        $form['label']['#default_value'] = $configuration['label'] ?? '';
        $form['description']['#default_value'] = $configuration['description'] ?? '';
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $this->condition->validateConfigurationForm($form['plugin']['settings'], $subform_state, $this->provideContext());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity instanceof ConfigEntityInterface) {
      $subform_state = SubformState::createForSubform($form['plugin']['settings'], $form, $form_state);
      $this->condition->submitConfigurationForm($form['plugin']['settings'], $subform_state, $this->provideContext());
      $values = $form_state->getValues();
      $entity_type_id = $this->entity->getEntityTypeId();
      // Now, setting common values for add/edit.
      $this->condition->setLabel($values['label']);
      $this->condition->setDescription($values['description']);
      // We need to determine if it's a "create" or "update" form.
      $operation = $this->conditionOperation();
      switch ($operation) {
        case 'create':
          $this->handler->addCondition($this->condition);
          $this->entity->save();
          $this->messenger()->addStatus($this->t('The condition was successfully added.'));
          break;

        case 'update':
          $this->handler->editCondition($values['condition'], $this->condition);
          $this->entity->save();
          $this->messenger()->addStatus($this->t('The condition was successfully updated.'));
          break;
      }
      $form_state->setRedirectUrl(
        Url::fromRoute(
          "entity.{$entity_type_id}.handler.conditions",
          [
            'entity' => $this->entity->id(),
            'handler' => $values['handler'],
          ]
        )
      );
    }
  }

  /**
   * Prepare a condition instance. Used to add or edit.
   *
   * @param string $condition_id
   *   The condition to be initialized.
   * @param string $op
   *   Use "new" if new instance, or "existing" for an existing one.
   *
   * @return \Drupal\auctioneer\ConditionInterface
   *   The condition instance.
   */
  protected function prepareConditionInstance(string $condition_id, string $op) {
    $condition = NULL;
    switch ($op) {
      case 'new':
        $condition = $this->conditionPluginManager->createInstance($condition_id);
        $existing_conditions = $this->handler->getConditions();
        $weight = 0;
        foreach ($existing_conditions as $existing_condition) {
          if ($weight <= $existing_condition->getWeight()) {
            $weight = $existing_condition->getWeight() + 1;
          }
        }
        $condition->setWeight($weight);
        break;

      case 'existing':
        $condition = $this->handler->getCondition($condition_id);
        break;
    }

    return $condition;
  }

  /**
   * Helper function to determine the nature of this form.
   *
   * @return string
   *   "create" or "update" depending on used form.
   */
  protected function conditionOperation() {
    return '';
  }

  /**
   * Provide common values for certain operations.
   *
   * @return array
   *   The needed values.
   */
  protected function provideContext() : array {
    return [
      'config_entity' => $this->entity ?? NULL,
      'handler' => $this->handler ?? NULL,
    ];
  }

}
