<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\auctioneer\AuctionTypeManagerInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer\Event\HammerBidEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the form to mark a bid as winner.
 */
class HammerBidForm extends FormBase {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs the "HammerBidForm" form.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->auctionTypeManager = $auction_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('auctioneer.auction_type.manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Workaround for multiple form instances.
    // https://www.drupal.org/project/drupal/issues/2821852#comment-13117654.
    static $count = 0;

    return 'auctioneer_hammer_bid_form_' . $count++;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = auctioneer_get_children($build_info, 'args->0->bid');
    if ($bid instanceof BidInterface) {
      /** @var \Drupal\auctioneer\Entity\AuctionInterface $auction */
      $auction = $bid->getAuction();
      // Setting bid object globally.
      $form_state->set('bid', $bid);
      // Other needed elements.
      $hammer = $auction instanceof AuctionInterface ? $this->auctionTypeManager->getAuctionHammerBid($auction) : NULL;
      $form['actions'] = [
        '#type' => 'actions',
      ];
      if ($hammer instanceof BidInterface && $hammer->id() === $bid->id()) {
        $form['actions']['unhammer'] = [
          '#type' => 'submit',
          '#value' => $this->t('Unset as winner'),
          '#op' => 'unhammer',
        ];
      }
      elseif (!$hammer instanceof BidInterface) {
        $form['actions']['hammer'] = [
          '#type' => 'submit',
          '#value' => $this->t('Set this bid as winner'),
          '#op' => 'hammer',
        ];
      }
      else {
        $form['hammer_exists'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Auction already has a winner.'),
          '#attributes' => [
            'class' => ['auction-existing-hammer'],
          ],
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();
    $bid = $form_state->get('bid');
    if ($bid instanceof BidInterface) {
      $action = NULL;
      if (isset($triggering['#op']) && $triggering['#op'] == 'hammer') {
        $this->auctionTypeManager->setAuctionHammerBid($bid);
        $this->messenger()->addMessage($this->t('Bid was successfully marked as winner!'));
        $action = $triggering['#op'];
      }
      elseif (isset($triggering['#op']) && $triggering['#op'] == 'unhammer') {
        $this->auctionTypeManager->deleteAuctionHammerBidData($bid);
        $this->messenger()->addMessage($this->t('Bid was successfully unmarked as winner!'));
        $action = $triggering['#op'];
      }
      else {
        // Just in case.
        $this->messenger()->addMessage($this->t('Unknown option!'), 'error');
      }
      // Dispatching event.
      if ($action) {
        $event = new HammerBidEvent($bid, $action);
        $this->eventDispatcher->dispatch($event, HammerBidEvent::EVENT_NAME);
      }
      $this->invalidateCacheTags($bid);
    }
  }

  /**
   * Helper function to invalidate cache tags.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid to trigger this action.
   */
  public function invalidateCacheTags(BidInterface $bid) {
    Cache::invalidateTags([
      'bid:' . $bid->id(),
      'bid_list',
      'bid_list:' . $bid->bundle(),
    ]);
  }

}
