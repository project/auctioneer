<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Url;

/**
 * Provides a bid type form to delete handler conditions.
 */
class BidTypeHandlerConditionDeleteForm extends HandlerConditionDeleteBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'auctioneer_bid_type_handler_condition_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity_type_id = $this->entity->getEntityTypeId();
    return Url::fromRoute(
      "entity.{$entity_type_id}.handler.conditions",
      [
        'entity' => $this->entity->id(),
        'handler' => $this->handler,
      ]
    );
  }

}
