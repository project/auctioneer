<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to add auction handlers.
 */
class AuctionTypeHandlerAddForm extends HandlerConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_auction_type_handler_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function handlerOperation() {
    return 'create';
  }

}
