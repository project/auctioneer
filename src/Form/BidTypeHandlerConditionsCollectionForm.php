<?php

namespace Drupal\auctioneer\Form;

/**
 * Form to manage handler conditions.
 */
class BidTypeHandlerConditionsCollectionForm extends HandlerConditionsCollectionBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_bid_type_handler_conditions_collection_form';
  }

}
