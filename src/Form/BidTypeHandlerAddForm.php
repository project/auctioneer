<?php

namespace Drupal\auctioneer\Form;

/**
 * Provides a form to add bid handlers.
 */
class BidTypeHandlerAddForm extends HandlerConfigurationBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auctioneer_bid_type_handler_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function handlerOperation() {
    return 'create';
  }

}
