<?php

namespace Drupal\auctioneer\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\auctioneer\AuctionTypeManagerInterface;

/**
 * Bid type entity class.
 */
class BidTypeEntityForm extends BundleEntityFormBase {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs the "BidTypeEntityForm" form.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager, MessengerInterface $messenger) {
    $this->auctionTypeManager = $auction_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('auctioneer.auction_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $bid_type = $this->entity;
    // Our form with custom properties/fields.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $bid_type->label(),
      '#description' => $this->t('Label for the %content_entity_id entity type (bundle).', ['%content_entity_id' => $bid_type->getEntityType()->getBundleOf()]),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $bid_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\auctioneer\Entity\BidType::load',
      ],
      '#disabled' => !$bid_type->isNew(),
    ];
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Auction plugin'),
      '#default_value' => $bid_type->getPluginName(),
      '#options' => $this->auctionTypeManager->getAvailableAuctionTypeNames(),
      '#required' => TRUE,
      '#disabled' => !$bid_type->isNew(),
      '#description' => $this->t('Select auction plugin to initialize needed base fields.'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Bid type description.'),
      '#default_value' => $bid_type->getDescription(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $bid_type = $this->entity;
    $status = $bid_type->save();
    if ($status == SAVED_NEW) {
      $auction_plugin_id = $form_state->getValue('plugin');
      // Let's set our plugin settings.
      $bid_type = $this->auctionTypeManager->initializeBidTypeBaseConfigurations($auction_plugin_id, $bid_type);
      // Finally, just a typical notification.
      $this->messenger->addMessage($this->t('Created the %label %content_entity_id bid type.', [
        '%label' => $bid_type->label(),
        '%content_entity_id' => $bid_type->getEntityType()->getBundleOf(),
      ]));
    }
    else {
      $this->messenger->addMessage($this->t('Saved the %label %content_entity_id bid type.', [
        '%label' => $bid_type->label(),
        '%content_entity_id' => $bid_type->getEntityType()->getBundleOf(),
      ]));
    }
    $form_state->setRedirectUrl($bid_type->toUrl('collection'));
  }

}
