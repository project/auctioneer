<?php

namespace Drupal\auctioneer\Form;

/**
 * Form to manage bid type handlers.
 */
class BidTypeHandlersCollectionForm extends HandlersCollectionBaseForm {}
