<?php

namespace Drupal\auctioneer\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for auction entities.
 */
class AuctionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['auctioneer_auction_statistics']['table']['group'] = $this->t('Auction statistics');
    $data['auctioneer_auction_statistics']['table']['join']['auction_field_data'] = [
      'type' => 'LEFT',
      'left_field' => 'id',
      'field' => 'auction_id',
    ];
    $data['auctioneer_auction_statistics']['total_amount_bids'] = [
      'title' => $this->t('Amount of bids'),
      'help' => $this->t('The total number of placed bids.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];
    $data['auctioneer_auction_statistics']['timestamp'] = [
      'title' => $this->t('Last interaction timestamp'),
      'help' => $this->t('Date and time of when the last interaction was made.'),
      'field' => [
        'id' => 'date',
      ],
      'sort' => [
        'id' => 'date',
      ],
      'filter' => [
        'id' => 'date',
      ],
    ];
    $data['auctioneer_auction_statistics']['initial_bid'] = [
      'title' => $this->t('Initial bid'),
      'help' => $this->t('The initial (first) bid placed to the auction.'),
      'relationship' => [
        'title' => $this->t('Initial bid'),
        'base' => 'bid_field_data',
        'base field' => 'id',
        'id' => 'standard',
        'label' => $this->t('Initial bid'),
      ],
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];
    $data['auctioneer_auction_statistics']['last_bid'] = [
      'title' => $this->t('Last bid'),
      'help' => $this->t('The last bid placed to the auction.'),
      'relationship' => [
        'title' => $this->t('Last bid'),
        'base' => 'bid_field_data',
        'base field' => 'id',
        'id' => 'standard',
        'label' => $this->t('Last bid'),
      ],
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];
    $data['auctioneer_auction_statistics']['context'] = [
      'title' => $this->t('Context'),
      'help' => $this->t('Comma-separated list of contexts.'),
      'field' => [
        'id' => 'standard',
      ],
      'sort' => [
        'id' => 'string',
      ],
      'filter' => [
        'id' => 'string',
      ],
    ];
    $data['auctioneer_auction_hammer']['table']['group'] = $this->t('Hammer', [], ['context' => 'auction hammer bids']);
    $data['auctioneer_auction_hammer']['table']['join']['auction_field_data'] = [
      'type' => 'LEFT',
      'left_field' => 'id',
      'field' => 'auction',
    ];
    $data['auctioneer_auction_hammer']['bid'] = [
      'title' => $this->t('Hammer bid'),
      'help' => $this->t('The hammer-marked (winner) bid.'),
      'relationship' => [
        'title' => $this->t('Hammer bid'),
        'base' => 'bid_field_data',
        'base field' => 'id',
        'id' => 'standard',
        'label' => $this->t('Hammer bid'),
      ],
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    return $data;
  }

}
