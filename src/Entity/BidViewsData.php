<?php

namespace Drupal\auctioneer\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for bid entities.
 */
class BidViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['auctioneer_auction_hammer']['table']['group'] = $this->t('Bid');
    $data['auctioneer_auction_hammer']['table']['join']['bid_field_data'] = [
      'type' => 'LEFT',
      'left_field' => 'id',
      'field' => 'bid',
    ];
    $data['auctioneer_auction_hammer']['is_hammer_bid'] = [
      'title' => $this->t('Is hammer bid'),
      'help' => $this->t('Indicates if current bid is hammer-marked.'),
      'field' => [
        'id' => 'is_hammer_bid',
      ],
      'filter' => [
        'id' => 'is_hammer_bid',
      ],
    ];
    $data['auctioneer_auction_hammer']['uid'] = [
      'title' => $this->t('Hammer author'),
      'help' => $this->t('The user who marked this bid as winner.'),
      'relationship' => [
        'title' => $this->t('Hammer author'),
        'base' => 'users_field_data',
        'base field' => 'uid',
        'id' => 'standard',
        'label' => $this->t('Hammer author'),
      ],
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    return $data;
  }

}
