<?php

namespace Drupal\auctioneer\Entity;

/**
 * Auction Type.
 *
 * @ConfigEntityType(
 *   id = "auction_type",
 *   label = @Translation("Auction Type"),
 *   label_collection = @Translation("Auction types"),
 *   label_singular = @Translation("Auction type"),
 *   label_plural = @Translation("Auction types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count auction type",
 *     plural = "@count auction types",
 *   ),
 *   bundle_of = "auction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "auction_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "bid_type",
 *     "description",
 *     "locked",
 *     "handlers"
 *   },
 *   handlers = {
 *     "access" = "Drupal\auctioneer\AuctionBundleAccessControlHandler",
 *     "list_builder" = "Drupal\auctioneer\Controller\AuctionTypesListBuilder",
 *     "form" = {
 *       "add" = "Drupal\auctioneer\Form\AuctionTypeEntityForm",
 *       "edit" = "Drupal\auctioneer\Form\AuctionTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "handlers" = "Drupal\auctioneer\Form\AuctionTypeHandlersCollectionForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer auction_type",
 *   links = {
 *     "add-form" = "/admin/structure/auctioneer/auction_type/add",
 *     "edit-form" = "/admin/structure/auctioneer/auction_type/{auction_type}/edit",
 *     "delete-form" = "/admin/structure/auctioneer/auction_type/{auction_type}/delete",
 *     "handlers" = "/admin/structure/auctioneer/auction_type/{auction_type}/handlers",
 *     "collection" = "/admin/structure/auctioneer/auction_type",
 *   }
 * )
 */
class AuctionType extends AuctioneerConfigEntityBundle implements AuctionTypeInterface {

  /**
   * The bid type ID.
   *
   * @var string
   */
  protected $bid_type;

  /**
   * The auction type description.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether the bundle is locked, indicating that it cannot be deleted.
   *
   * @var bool
   */
  protected $locked = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getBidTypeName() {
    return $this->bid_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getBidType() {
    return $this->bid_type ? BidType::load($this->bid_type) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setBidTypeName($bid_type_id) {
    $this->bid_type = $bid_type_id;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    return (bool) $this->locked;
  }

  /**
   * {@inheritdoc}
   */
  public function lock() {
    $this->locked = TRUE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unlock() {
    $this->locked = FALSE;

    return $this;
  }

  /**
   * Get auction type description.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set auction type description.
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

}
