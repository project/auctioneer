<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Base class for common auction entities like bids and auctions.
 */
abstract class AuctioneerContentEntity extends ContentEntityBase implements AuctioneerContentEntityInterface {

  /**
   * Helper method to execute handlers.
   *
   * @param string $operation
   *   The operation to execute handlers.
   */
  public function performHandlersExecution(string $operation) {
    $entity_type = $this->entityTypeManager()
      ->getStorage($this->getEntityType()->getBundleEntityType())
      ->load($this->bundle());
    $entity_type->executeHandlers(
      $this,
      $operation
    );
  }

}
