<?php

namespace Drupal\auctioneer\Entity;

/**
 * Bid Type.
 *
 * @ConfigEntityType(
 *   id = "bid_type",
 *   label = @Translation("Bid Type"),
 *   bundle_of = "bid",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "bid_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "plugin",
 *     "handlers"
 *   },
 *   handlers = {
 *     "access" = "Drupal\auctioneer\BidBundleAccessControlHandler",
 *     "list_builder" = "Drupal\auctioneer\Controller\BidTypesListBuilder",
 *     "form" = {
 *       "default" = "Drupal\auctioneer\Form\BidTypeEntityForm",
 *       "add" = "Drupal\auctioneer\Form\BidTypeEntityForm",
 *       "edit" = "Drupal\auctioneer\Form\BidTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "handlers" = "Drupal\auctioneer\Form\BidTypeHandlersCollectionForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer bid_type",
 *   links = {
 *     "add-form" = "/admin/structure/auctioneer/bid_type/add",
 *     "edit-form" = "/admin/structure/auctioneer/bid_type/{bid_type}/edit",
 *     "delete-form" = "/admin/structure/auctioneer/bid_type/{bid_type}/delete",
 *     "handlers" = "/admin/structure/auctioneer/bid_type/{bid_type}/handlers",
 *     "collection" = "/admin/structure/auctioneer/bid_type"
 *   }
 * )
 */
class BidType extends AuctioneerConfigEntityBundle implements BidTypeInterface {

  /**
   * The bid type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The bid type ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getPluginName() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginName($plugin) {
    $this->plugin = $plugin;

    return $this;
  }

  /**
   * Get bid type description.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set bid type description.
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

}
