<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for auction types.
 */
interface AuctionTypeInterface extends ConfigEntityInterface {

  /**
   * Get bid type name.
   *
   * @return string
   *   The bid type ID.
   */
  public function getBidTypeName();

  /**
   * Get bid type configuration entity.
   *
   * @return \Drupal\auctioneer\Entity\BidTypeInterface
   *   The bid type configuration entity.
   */
  public function getBidType();

  /**
   * Set bid type name.
   *
   * @param string $bid_type_id
   *   The bid type ID.
   *
   * @return $this
   */
  public function setBidTypeName($bid_type_id);

  /**
   * Gets whether the bundle is locked.
   *
   * Locked bundles cannot be deleted.
   *
   * @return bool
   *   TRUE if the bundle is locked, FALSE otherwise.
   */
  public function isLocked();

  /**
   * Locks the bundle.
   *
   * @return $this
   */
  public function lock();

  /**
   * Unlocks the bundle.
   *
   * @return $this
   */
  public function unlock();

}
