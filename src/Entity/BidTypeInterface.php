<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for bid types.
 */
interface BidTypeInterface extends ConfigEntityInterface {

  /**
   * Get plugin ID.
   *
   * @return string
   *   The auction type plugin ID.
   */
  public function getPluginName();

  /**
   * Set auction plugin ID.
   *
   * @param string $plugin
   *   The auction type plugin ID.
   *
   * @return $this
   */
  public function setPluginName($plugin);

}
