<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;

/**
 * Defines the "auction" entity.
 *
 * @ContentEntityType(
 *   id = "auction",
 *   label = @Translation("Auction"),
 *   label_collection = @Translation("Auctions"),
 *   label_singular = @Translation("Auction"),
 *   label_plural = @Translation("Auctions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Auction",
 *     plural = "@count Auctions",
 *   ),
 *   bundle_label = @Translation("Auction type"),
 *   base_table = "auction",
 *   data_table = "auction_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "published" = "status",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "list_builder" = "Drupal\auctioneer\Controller\AuctionsListBuilder",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "views_data" = "Drupal\auctioneer\Entity\AuctionViewsData",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\auctioneer\Form\AuctionForm",
 *       "add" = "Drupal\auctioneer\Form\AuctionForm",
 *       "edit" = "Drupal\auctioneer\Form\AuctionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *   },
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   links = {
 *     "canonical" = "/auction/{auction}",
 *     "add-page" = "/auction/add",
 *     "add-form" = "/auction/add/{auction_type}",
 *     "edit-form" = "/auction/{auction}/edit",
 *     "delete-form" = "/auction/{auction}/delete",
 *     "collection" = "/admin/structure/auctioneer/auctions",
 *   },
 *   admin_permission = "administer auction",
 *   permission_granularity = "bundle",
 *   bundle_entity_type = "auction_type",
 *   field_ui_base_route = "entity.auction_type.edit_form",
 * )
 */
class Auction extends AuctioneerContentEntity implements AuctionInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * The common/needed field definitions for all auction types.
   *
   * Rest of fields will be provided by each plugin.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type interface.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('Authored by'))
      ->setDescription(t('The auction author.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The auction title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The auction URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['status']
      ->setLabel(t('Published'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 90,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the auction was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the auction was last edited.'));

    $fields['auction_state'] = BaseFieldDefinition::create('list_string')
      ->setSettings([
        'allowed_values_function' => '\Drupal\auctioneer\AuctionTypeManager::getAllowedAuctionStates',
      ])
      ->setLabel('Auction state')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuctionState() {
    return $this->get('auction_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuctionType() {
    return AuctionType::load($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstBid() {
    /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $bid_storage */
    $bid_storage = $this->entityTypeManager()->getStorage('bid');
    $result = $bid_storage->getQuery()
      ->condition('bundle', $this->getAuctionType()->getBidTypeName())
      ->condition('auction_id', $this->id())
      ->accessCheck(FALSE)
      ->sort('created', 'ASC')
      ->range(0, 1)
      ->execute();

    return !empty($result) ? $bid_storage->load(reset($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastBid() {
    /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $bid_storage */
    $bid_storage = $this->entityTypeManager()->getStorage('bid');
    $result = $bid_storage->getQuery()
      ->condition('bundle', $this->getAuctionType()->getBidTypeName())
      ->condition('auction_id', $this->id())
      ->accessCheck(FALSE)
      ->sort('created', 'DESC')
      ->range(0, 1)
      ->execute();

    return !empty($result) ? $bid_storage->load(reset($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRandomBid() {
    /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $bid_storage */
    $bid_storage = $this->entityTypeManager()->getStorage('bid');
    $result = $bid_storage->getQuery()
      ->condition('bundle', $this->getAuctionType()->getBidTypeName())
      ->condition('auction_id', $this->id())
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->addTag('auctioneer_auction__random_bid')
      ->execute();

    return !empty($result) ? $bid_storage->load(reset($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->performHandlersExecution(
      $update ? 'update' : 'create'
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    foreach ($entities as $entity) {
      $entity->performHandlersExecution('delete');
    }
  }

}
