<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for auctions.
 */
interface AuctionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Get auction state.
   *
   * @return string
   *   The auction state.
   */
  public function getAuctionState();

  /**
   * Get auction type configuration entity.
   *
   * @return \Drupal\auctioneer\Entity\AuctionType
   *   The auction type configuration entity.
   */
  public function getAuctionType();

  /**
   * Get first bid on this auction.
   */
  public function getFirstBid();

  /**
   * Get last bid on this auction.
   */
  public function getLastBid();

  /**
   * Get a random bid on this auction.
   */
  public function getRandomBid();

}
