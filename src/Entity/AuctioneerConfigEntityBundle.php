<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\auctioneer\HandlerPluginsCollection;

/**
 * Base class for common auction entity types like bid types and auction types.
 */
abstract class AuctioneerConfigEntityBundle extends ConfigEntityBundleBase implements EntityWithPluginCollectionInterface {

  /**
   * The array of handlers.
   *
   * @var array
   */
  protected $handlers = [];

  /**
   * Holds the collection of handler plugins.
   *
   * @var \Drupal\auctioneer\HandlerPluginsCollection
   */
  protected $handlersCollection;

  /**
   * Get available handlers in current entity type.
   *
   * @param array $trigering_events
   *   Optionally limit handlers only to the spcified events.
   * @param bool $status
   *   Optionally filter by status.
   * @param string $plugin_id
   *   Optionally filter by plugin_id.
   *
   * @return \Drupal\auctioneer\HandlerPluginsCollection
   *   The collection of handler plugins.
   */
  public function getHandlers(array $trigering_events = [], bool $status = NULL, string $plugin_id = NULL) {
    if (!$this->handlersCollection) {
      $this->handlersCollection = new HandlerPluginsCollection($this->getHandlersPluginManager(), $this->handlers);
      $this->handlersCollection->sort();
    }
    $handlers = $this->handlersCollection;
    if (!empty($trigering_events) || isset($plugin_id) || isset($status)) {
      $handlers = clone $this->handlersCollection;
      // Filter by events.
      if (!empty($trigering_events)) {
        foreach ($handlers as $instance_id => $handler) {
          $common_triggering = array_intersect($handler->getEvents(), $trigering_events);
          if (empty($common_triggering)) {
            $handlers->removeInstanceId($instance_id);
          }
        }
      }
      // Filter by status.
      if (isset($status)) {
        foreach ($handlers as $instance_id => $handler) {
          if ($handler->getStatus() !== $status) {
            $handlers->removeInstanceId($instance_id);
          }
        }
      }
      // Filter by plugin ID.
      if (isset($plugin_id)) {
        foreach ($handlers as $instance_id => $handler) {
          if ($handler->getPluginId() !== $plugin_id) {
            $handlers->removeInstanceId($instance_id);
          }
        }
      }
    }

    return $handlers;
  }

  /**
   * Add a new handler.
   *
   * @param array $configuration
   *   The new handler configuration.
   *
   * @return string
   *   The added instance ID.
   */
  public function addHandler(array $configuration) {
    $id = $this->uuidGenerator()->generate();
    $this->getHandlers()->addInstanceId($id, $configuration);

    return $id;
  }

  /**
   * Edit a specified handler.
   *
   * @param string $instance_id
   *   The handler instance ID.
   * @param array $configuration
   *   The updated configuration.
   *
   * @return \Drupal\auctioneer\Entity\AuctioneerConfigEntityBundle
   *   The current configuration entity object.
   */
  public function editHandler(string $instance_id, array $configuration) {
    $this->getHandlers()->setInstanceConfiguration($instance_id, $configuration);

    return $this;
  }

  /**
   * Remove a specified handler.
   *
   * @param string $instance_id
   *   The handler instance ID.
   *
   * @return \Drupal\auctioneer\Entity\AuctioneerConfigEntityBundle
   *   The current configuration entity object.
   */
  public function removeHandler(string $instance_id) {
    $this->getHandlers()->removeInstanceId($instance_id);

    return $this;
  }

  /**
   * Helper method to get plugin manager.
   *
   * @return mixed|object|null
   *   The handlers plugin manager.
   */
  protected function getHandlersPluginManager() {
    return \Drupal::service('plugin.manager.auctioneer.handler');
  }

  /**
   * Get single handler instance.
   *
   * @param string $instance_id
   *   The handler instance ID.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The handler instance.
   */
  public function getHandler(string $instance_id) {
    return $this->getHandlers()->get($instance_id);
  }

  /**
   * Perform handlers execution.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity on which the handlers will be executed.
   * @param string $operation
   *   Trigering event on given entity.
   */
  public function executeHandlers(ContentEntityInterface $entity, string $operation) {
    $handlers = $this->getHandlers([$operation], TRUE);
    foreach ($handlers as $instance_id => $handler) {
      if ($handler->validateConditions($entity)) {
        $handler->execute(
          $entity,
          [
            'operation' => $operation,
            'instance_id' => $instance_id,
          ]
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['handlers' => $this->getHandlers()];
  }

}
