<?php

namespace Drupal\auctioneer\Entity;

/**
 * Provides an interface for bids and auctions.
 */
interface AuctioneerContentEntityInterface {}
