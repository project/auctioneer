<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for bids.
 */
interface BidInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Get auction where this bid was placed on.
   */
  public function getAuction();

  /**
   * Get bid type configuration entity.
   */
  public function getBidType();

}
