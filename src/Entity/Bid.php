<?php

namespace Drupal\auctioneer\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the "bid" entity.
 *
 * @ContentEntityType(
 *   id = "bid",
 *   label = @Translation("Bid"),
 *   label_collection = @Translation("Bids"),
 *   label_singular = @Translation("Bid"),
 *   label_plural = @Translation("Bids"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Bid",
 *     plural = "@count Bids",
 *   ),
 *   bundle_label = @Translation("Bid type"),
 *   base_table = "bid",
 *   data_table = "bid_field_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "bundle" = "bundle",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *   },
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "list_builder" = "Drupal\auctioneer\Controller\BidsListBuilder",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "views_data" = "Drupal\auctioneer\Entity\BidViewsData",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\auctioneer\Form\BidForm",
 *       "add" = "Drupal\auctioneer\Form\BidForm",
 *       "edit" = "Drupal\auctioneer\Form\BidForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   links = {
 *     "add-page" = "/auctioner/bid/add",
 *     "add-form" = "/auctioner/bid/add/{bid_type}",
 *     "edit-form" = "/auctioner/bid/{bid}/edit",
 *     "delete-form" = "/auctioner/bid/{bid}/delete",
 *     "collection" = "/admin/structure/auctioneer/bids",
 *   },
 *   admin_permission = "administer bid",
 *   permission_granularity = "bundle",
 *   bundle_entity_type = "bid_type",
 *   field_ui_base_route = "entity.bid_type.edit_form",
 * )
 */
class Bid extends AuctioneerContentEntity implements BidInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;
  use StringTranslationTrait;

  /**
   * The common/needed field definitions for all bid types.
   *
   * Rest of fields will be provided by each plugin.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type interface.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['auction_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Auction'))
      ->setDescription(t('The parent auction.'))
      ->setSetting('target_type', 'auction')
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid']
      ->setLabel(t('Authored by'))
      ->setDescription(t('The bid author.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the bid was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the bid was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuction() {
    $value = $this->get('auction_id')->referencedEntities();

    return reset($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getBidType() {
    return BidType::load($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->t('Bid');
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->performHandlersExecution(
      $update ? 'update' : 'create'
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    foreach ($entities as $entity) {
      $entity->performHandlersExecution('delete');
    }
  }

}
