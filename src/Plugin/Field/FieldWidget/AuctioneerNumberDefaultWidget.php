<?php

namespace Drupal\auctioneer\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'auctioneer_number_default' widget.
 *
 * @FieldWidget(
 *   id = "auctioneer_number_default",
 *   label = @Translation("Auctioneer number"),
 *   field_types = {
 *     "auctioneer_number"
 *   }
 * )
 */
class AuctioneerNumberDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'auctioneer_number';
    if (!$items[$delta]->isEmpty()) {
      $element['#default_value'] = $items[$delta]->getValue();
    }

    return $element;
  }

}
