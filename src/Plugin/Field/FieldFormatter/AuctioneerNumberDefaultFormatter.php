<?php

namespace Drupal\auctioneer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'auctioneer_number_default' formatter.
 *
 * @FieldFormatter(
 *   id = "auctioneer_number_default",
 *   label = @Translation("Auctioneer number"),
 *   field_types = {
 *     "auctioneer_number"
 *   }
 * )
 */
class AuctioneerNumberDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $item->number,
        '#cache' => [
          'contexts' => [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
          ],
        ],
      ];
    }

    return $elements;
  }

}
