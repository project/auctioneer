<?php

namespace Drupal\auctioneer\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'auctioneer_number' field type.
 *
 * @FieldType(
 *   id = "auctioneer_number",
 *   label = @Translation("Auctioneer number"),
 *   description = @Translation("Stores an un-changed number."),
 *   category = @Translation("Auctioneer"),
 *   default_widget = "auctioneer_number_default",
 *   default_formatter = "auctioneer_number_default",
 * )
 */
class AuctioneerNumber extends FieldItemBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['number'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 19,
      'allow_negative' => FALSE,
      'allow_decimal' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'number' => [
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
          'binary' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    $max_length = $this->getSetting('max_length');
    $allow_negative = $this->getSetting('allow_negative');
    $allow_decimal = $this->getSetting('allow_decimal');
    if ($max_length) {
      $field_label = $this->getFieldDefinition()->getLabel();
      if ($allow_negative && $allow_decimal) {
        // Allowed integer, negative and decimals.
        $regex_pattern = '/^(([-]?)|(\d?))((\d+(\.\d*)?)|(\d+))?\d$/';
      }
      elseif (!$allow_negative && $allow_decimal) {
        // Allowed positive integer and decimals only.
        $regex_pattern = '/^((\d+(\.\d*)?)|(\d+))?\d$/';
      }
      elseif ($allow_negative && !$allow_decimal) {
        // Allow positive and negative integers only.
        $regex_pattern = '/^(([-]?)|(\d?))(\d+)$/';
      }
      else {
        // Only positive integers.
        $regex_pattern = '/^(\d+)$/';
      }
      $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
        'number' => [
          'Length' => [
            'max' => $max_length,
            'maxMessage' => $this->t(
              '%field_label: may not be longer than @max digits.',
              ['%field_label' => $field_label, '@max' => $max_length]
            ),
          ],
          'Regex' => [
            'pattern' => $regex_pattern,
            'message' => $this->t('%field_label is not a valid number.', ['%field_label' => $field_label]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $value = [
      'number' => '3.333',
    ];

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [
      'max_length' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum length'),
        '#default_value' => $this->getSetting('max_length'),
        '#required' => TRUE,
        '#description' => $this->t('The maximum length of the field in characters, including sing (for negative values) and dot (for decimal digits).'),
        '#min' => 1,
        '#disabled' => $has_data,
      ],
      'allow_negative' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Allow negative values'),
        '#default_value' => $this->getSetting('allow_negative'),
        '#disabled' => $has_data,
      ],
      'allow_decimal' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Allow decimal values'),
        '#default_value' => $this->getSetting('allow_decimal'),
        '#disabled' => $has_data,
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values['number'])) {
      $values = [
        'number' => $values['number'],
      ];
    }
    parent::setValue($values, $notify);
  }

}
