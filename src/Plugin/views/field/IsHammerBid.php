<?php

namespace Drupal\auctioneer\Plugin\views\field;

use Drupal\views\Plugin\views\field\Boolean;

/**
 * Bid is a hammer bid.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("is_hammer_bid")
 */
class IsHammerBid extends Boolean {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $this->field_alias = $this->query->addField(NULL, "COALESCE({$this->tableAlias}.hid, 0) > 0", $this->tableAlias . '_' . $this->field);
  }

}
