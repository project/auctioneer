<?php

namespace Drupal\auctioneer\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Bid is a hammer bid.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("is_hammer_bid")
 */
class IsHammerBid extends BooleanOperator {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $value = $this->value;
    $expression = (bool) $value ? 'IS NOT NULL' : 'IS NULL';
    // Append our expression itself.
    $this->query->addWhereExpression($this->options['group'], "{$this->tableAlias}.hid $expression");
  }

}
