<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * List available handlers to add.
 */
class HandlerSelectionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The handler plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $handlerPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->handlerPluginManager = $container->get('plugin.manager.auctioneer.handler');

    return $instance;
  }

  /**
   * Build available list of handler plugins for bid types.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $bid_type
   *   The bid type.
   */
  public function listAvailableBidHandlers(Request $request, ConfigEntityInterface $bid_type) {
    return $this->buildAvailableHandlerPluginsList($bid_type);
  }

  /**
   * Build available list of handler plugins for auction types.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $auction_type
   *   The auction type.
   */
  public function listAvailableAuctionHandlers(Request $request, ConfigEntityInterface $auction_type) {
    return $this->buildAvailableHandlerPluginsList($auction_type);
  }

  /**
   * Build a list of available handler plugins for a specified entity type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   Bid or Auction type.
   */
  protected function buildAvailableHandlerPluginsList(ConfigEntityInterface $entity) {
    $header = [
      ['data' => $this->t('Title')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Plugin ID')],
      ['data' => $this->t('Operations')],
    ];
    $rows = [];
    $entity_type_id = $entity->getEntityTypeId();
    foreach ($this->handlerPluginManager->getDefinitions() as $plugin_definition) {
      $plugin_scopes = explode(',', str_replace(' ', '', $plugin_definition['scope']));
      $intersect = array_intersect(
        $plugin_scopes,
        [str_replace('_type', '', $entity_type_id)]
      );
      if (count($intersect) > 0) {
        $rows[] = [
          'title' => [
            'data' => [
              '#markup' => $plugin_definition['label'] ?? '',
            ],
          ],
          'description' => [
            'data' => [
              '#markup' => $plugin_definition['description'] ?? '',
            ],
          ],
          'plugin_id' => [
            'data' => [
              '#markup' => $plugin_definition['id'] ?? '',
            ],
          ],
          'operations' => [
            'data' => [
              '#type' => 'link',
              '#title' => $this->t('Add'),
              '#url' => Url::fromRoute(
                "entity.{$entity_type_id}.handler.add_form",
                [
                  'entity' => $entity->id(),
                  'handler' => $plugin_definition['id'],
                ]
              ),
              '#attributes' => [
                'class' => [
                  'button',
                ],
              ],
            ],
          ],
        ];
      }
    }
    $output = [
      'table' => [
        '#type' => 'table',
        '#header' => $header,
        '#empty' => $this->t('There are no available handler plugins available for this entity type.'),
      ] + $rows,
    ];

    return $output;
  }

}
