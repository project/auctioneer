<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a listing of bid entities.
 *
 * @ingroup auctioneer
 */
class BidsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'type' => $this->t('Type'),
      'auction' => $this->t('Auction'),
      'created' => $this->t('Created'),
      'changed' => $this->t('Changed'),
      'owner' => $this->t('Owner'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\auctioneer\Entity\BidTypeInterface $bid_type */
    $bid_type = $entity->getBidType();
    $auction = $entity->getAuction();
    $owner = $entity->getOwner();
    $row = [
      'type' => $bid_type->label(),
      'auction' => $auction ? [
        'data' => [
          '#type' => 'link',
          '#title' => $auction->label(),
        ] + $auction->toUrl()->toRenderArray(),
      ] : '',
      'created' => date('Y-m-d H:i:s', $entity->get('created')->getString()),
      'changed' => date('Y-m-d H:i:s', $entity->get('changed')->getString()),
      'owner' => $owner ? [
        'data' => [
          '#type' => 'link',
          '#title' => $owner->getDisplayName(),
        ] + $owner->toUrl()->toRenderArray(),
      ] : '',
    ];

    return $row + parent::buildRow($entity);
  }

}
