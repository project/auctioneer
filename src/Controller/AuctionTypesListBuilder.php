<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of auction_type entities.
 *
 * @ingroup auctioneer
 */
class AuctionTypesListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'auctioneer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Label'),
      'machine_name' => $this->t('Machine name'),
      'auction_bid_type_id' => $this->t('Bid type'),
      'auction_plugin_id' => $this->t('Plugin ID'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [
      'label' => $entity->label(),
      'machine_name' => $entity->id(),
      'auction_bid_type_id' => $entity->getBidTypeName(),
      'auction_plugin_id' => $entity->getBidType()->getPluginName(),
    ];

    return $row + parent::buildRow($entity);
  }

}
