<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of bid_types entities.
 *
 * @ingroup auctioneer
 */
class BidTypesListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'auctioneer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label' => $this->t('Label'),
      'machine_name' => $this->t('Machine name'),
      'plugin' => $this->t('Plugin'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [
      'label' => $entity->label(),
      'machine_name' => $entity->id(),
      'plugin' => $entity->getPluginName(),
    ];

    return $row + parent::buildRow($entity);
  }

}
