<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * List available conditions to add.
 */
class ConditionSelectionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $conditionPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->conditionPluginManager = $container->get('plugin.manager.auctioneer.condition');

    return $instance;
  }

  /**
   * Build available list of condition plugins for bid type handlers.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The bid type.
   * @param string $handler
   *   The handler instance being managed.
   */
  public function listAvailableBidHandlerConditions(Request $request, ConfigEntityInterface $entity, string $handler) {
    return $this->buildAvailableEntityTypeHandlerConditionsPluginsList($entity, $handler);
  }

  /**
   * Build available list of condition plugins for auction type handlers.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The auction type.
   * @param string $handler
   *   The handler instance being managed.
   */
  public function listAvailableAuctionHandlerConditions(Request $request, ConfigEntityInterface $entity, string $handler) {
    return $this->buildAvailableEntityTypeHandlerConditionsPluginsList($entity, $handler);
  }

  /**
   * Build a list of available handler plugins for a specified entity type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   Bid or Auction type.
   * @param string $handler
   *   The handler instance being managed.
   */
  protected function buildAvailableEntityTypeHandlerConditionsPluginsList(ConfigEntityInterface $entity, string $handler) {
    try {
      $handler_instance = $entity->getHandler($handler);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Handler ID not found: '$handler'.");
    }
    $entity_type_id = $entity->getEntityTypeId();
    $rows = [];
    foreach ($this->conditionPluginManager->getDefinitions() as $plugin_definition) {
      $plugin_scopes = explode(',', str_replace(' ', '', $plugin_definition['scope']));
      $intersect = array_intersect(
        $plugin_scopes,
        [str_replace('_type', '', $entity_type_id)]
      );
      if (count($intersect) > 0) {
        $rows[] = [
          'title' => [
            'data' => [
              '#markup' => $plugin_definition['label'] ?? '',
            ],
          ],
          'description' => [
            'data' => [
              '#markup' => $plugin_definition['description'] ?? '',
            ],
          ],
          'plugin_id' => [
            'data' => [
              '#markup' => $plugin_definition['id'] ?? '',
            ],
          ],
          'operations' => [
            'data' => [
              '#type' => 'link',
              '#title' => $this->t('Add'),
              '#url' => Url::fromRoute(
                "entity.{$entity_type_id}.handler.condition.add_form",
                [
                  'entity' => $entity->id(),
                  'handler' => $handler,
                  'condition' => $plugin_definition['id'],
                ]
              ),
              '#attributes' => [
                'class' => [
                  'button',
                ],
              ],
            ],
          ],
        ];
      }
    }
    $output = [
      'handler' => [
        '#type' => 'container',
        '#tree' => TRUE,
        'label' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $handler_instance->getLabel(),
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $handler_instance->getDescription(),
        ],
      ],
      'table' => [
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('Title')],
          ['data' => $this->t('Description')],
          ['data' => $this->t('Plugin ID')],
          ['data' => $this->t('Operations')],
        ],
        '#empty' => $this->t('There are no available condition plugins available for this entity type.'),
      ] + $rows,
    ];

    return $output;
  }

}
