<?php

namespace Drupal\auctioneer\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\auctioneer\Entity\AuctionType;

/**
 * Provides a listing of auction entities.
 *
 * @ingroup auctioneer
 */
class AuctionsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'title' => $this->t('Title'),
      'type' => $this->t('Type'),
      'state' => $this->t('State'),
      'status' => $this->t('Status'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type */
    $auction_type = AuctionType::load($entity->bundle());
    $row = [
      'title' => [
        'data' => [
          '#type' => 'link',
          '#title' => $entity->label(),
        ] + $entity->toUrl()->toRenderArray(),
      ],
      'type' => $auction_type->label(),
      'state' => $entity->getAuctionState(),
      'status' => $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
    ];

    return $row + parent::buildRow($entity);
  }

}
