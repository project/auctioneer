<?php

namespace Drupal\auctioneer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines the interface for auction types.
 */
interface AuctionTypeInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Gets the auction base fields.
   *
   * @return array
   *   The auction entity base fields.
   */
  public function getAuctionBaseFieldDefinitions();

  /**
   * Gets the bid base fields.
   *
   * @return array
   *   The bid entity base fields.
   */
  public function getBidBaseFieldDefinitions();

  /**
   * Allow plugins to deal with bid form building.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function processBidForm(array &$form, FormStateInterface $form_state);

  /**
   * Allow plugins to deal with bid form validation.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function validateBidForm(array &$form, FormStateInterface $form_state);

  /**
   * Allow plugins to deal with bid form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function submitBidForm(array &$form, FormStateInterface $form_state);

}
