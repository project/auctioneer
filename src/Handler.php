<?php

namespace Drupal\auctioneer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Uuid\UuidInterface;

/**
 * Defines the base class for handlers.
 */
abstract class Handler extends PluginBase implements HandlerInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The label of this entity handler instance.
   *
   * @var string
   */
  protected $label = '';

  /**
   * The description of this entity handler instance.
   *
   * @var string
   */
  protected $description = '';

  /**
   * The status of this entity handler instance.
   *
   * @var bool
   */
  protected $status;

  /**
   * The weight of this entity handler instance.
   *
   * @var int|string
   */
  protected $weight = '';

  /**
   * The triggering events for this handler.
   *
   * @var array
   */
  protected $events = [];

  /**
   * The conditions for this handler.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * Holds the collection of condition plugins.
   *
   * @var \Drupal\auctioneer\ConditionPluginsCollection
   */
  protected $conditionsCollection = [];

  /**
   * Constructs a Handler plugin object.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $account, UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->uuidService = $uuid;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description) {
    $this->description = $description;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(bool $status) {
    $this->status = $status;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight) {
    $this->weight = $weight;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents() {
    return $this->events;
  }

  /**
   * {@inheritdoc}
   */
  public function setEvents(array $events) {
    $this->events = $events;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, ConfigEntityInterface $entity = NULL) {}

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity, array $context = []) {}

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration += [
      'label' => '',
      'description' => '',
      'status' => FALSE,
      'weight' => '',
      'events' => [],
      'conditions' => [
        'instances' => [],
        'cardinality' => '',
      ],
      'settings' => [],
    ];
    $this->label = $configuration['label'];
    $this->description = $configuration['description'];
    $this->status = $configuration['status'];
    $this->weight = $configuration['weight'];
    $this->events = $configuration['events'];
    $this->conditions = [
      'instances' => $configuration['conditions']['instances'] ?? [],
      'cardinality' => $configuration['conditions']['cardinality'] ?? 'AND',
    ];
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'label' => $this->getLabel(),
      'description' => $this->getDescription(),
      'status' => $this->getStatus(),
      'weight' => $this->getWeight(),
      'events' => $this->getEvents(),
      'conditions' => $this->getConditionsConfiguration(),
      'settings' => $this->configuration,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsPluginManager() {
    return \Drupal::service('plugin.manager.auctioneer.condition');
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsConfiguration() {
    return $this->conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    if (!$this->conditionsCollection) {
      $this->conditionsCollection = new ConditionPluginsCollection($this->getConditionsPluginManager(), $this->getConditionsConfiguration()['instances']);
      $this->conditionsCollection->sort();
    }

    return $this->conditionsCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsCardinality() {
    return $this->getConditionsConfiguration()['cardinality'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionsCardinality(string $cardiality) {
    $this->conditions['cardinality'] = $cardiality;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCondition(ConditionInterface $condition) {
    $this->performConditionOperation(
      'add',
      [
        'condition' => $condition,
      ]
    );

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function editCondition(string $instance_id, ConditionInterface $condition) {
    $this->performConditionOperation(
      'edit',
      [
        'instance_id' => $instance_id,
        'condition' => $condition,
      ]
    );

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeCondition(string $instance_id) {
    $this->performConditionOperation(
      'delete',
      [
        'instance_id' => $instance_id,
      ]
    );

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCondition(string $instance_id) {
    return $this->getConditions()->get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConditions(ContentEntityInterface $entity) : bool {
    $validation = FALSE;
    $conditions = $this->getConditions();
    // If no conditions, then handler must be executed.
    if ($conditions->count() === 0) {
      return TRUE;
    }
    else {
      $validations = [];
      $cardinality = $this->getConditionsCardinality();
      foreach ($conditions as $instance_id => $condition) {
        $validations[$instance_id] = $condition->evaluate([
          'entity' => $entity,
        ]);
      }
      switch ($cardinality) {
        case 'AND':
          $validation = !in_array(FALSE, array_values($validations));
          break;

        case 'OR':
          $validation = in_array(TRUE, array_values($validations));
          break;
      }
    }

    return $validation;
  }

  /**
   * Perform a condition operation.
   *
   * @param string $operation
   *   One of "add", "edit" or "delete".
   * @param array $data
   *   Needed parameters for this condition.
   */
  protected function performConditionOperation(string $operation, array $data) {
    $conditions = $this->getConditions();
    switch ($operation) {
      case 'add':
        if (
          isset($data['condition']) &&
          $data['condition'] instanceof ConditionInterface
        ) {
          $conditions->addInstanceId($this->uuidService->generate(), $data['condition']->getConfiguration());
        }
        break;

      case 'edit':
        if (
          isset($data['instance_id']) &&
          is_string($data['instance_id']) &&
          isset($data['condition']) &&
          $data['condition'] instanceof ConditionInterface
        ) {
          $conditions->setInstanceConfiguration($data['instance_id'], $data['condition']->getConfiguration());
        }
        break;

      case 'delete':
        if (
          isset($data['instance_id']) &&
          is_string($data['instance_id'])
        ) {
          $conditions->removeInstanceId($data['instance_id']);
        }
        break;
    }
    // Ensure a fresh/synced dataset.
    $this->conditions['instances'] = [];
    $this->conditionsCollection = $conditions;
    if ($this->conditionsCollection->count() > 0) {
      foreach ($this->conditionsCollection as $instance_id => $condition) {
        $this->conditions['instances'][$instance_id] = $condition->getConfiguration();
      }
    }
  }

}
