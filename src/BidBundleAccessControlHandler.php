<?php

namespace Drupal\auctioneer;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\BundleEntityAccessControlHandler;

/**
 * Defines the access control handler for bid bundles.
 */
class BidBundleAccessControlHandler extends BundleEntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // @todo Lock deleting if it's marked as winner
    if ($operation === 'delete' && FALSE) {
      return AccessResult::forbidden()->addCacheableDependency($entity);
    }
    else {
      return parent::checkAccess($entity, $operation, $account);
    }
  }

}
