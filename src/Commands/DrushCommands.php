<?php

namespace Drupal\auctioneer\Commands;

use Drush\Commands\DrushCommands as DrushCommandsBase;
use Drupal\auctioneer\AuctionTypeManagerInterface;

/**
 * Defines Drush commands for Auctioneer module.
 */
class DrushCommands extends DrushCommandsBase {

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager) {
    parent::__construct();
    $this->auctionTypeManager = $auction_type_manager;
  }

  /**
   * Purge orphan bids.
   *
   * @command auctioneer:purge_orphan_bids
   *
   * @option limit
   *   The amount of bids to process at time.
   *
   * @usage drush auctioneer:purge_orphan_bids
   *   Purge orphan bids. Basic usage.
   * @usage drush auctioneer:purge_orphan_bids --limit=xxx
   *   Purge orphan bids. Process an amount of xxx bids.
   *
   * @aliases auctioneer-pob
   */
  public function purgeOrphanBids(
    array $options = [
      'limit' => NULL,
    ]) {
    $conf = [
      'limit' => $options['limit'],
    ];
    $stats = $this->auctionTypeManager->purgeOrphanBids($conf);
    $this->output()->writeln(dt('An amount of @total orphan bids were deleted.', ['@total' => $stats['count']['_total']]));
  }

  /**
   * Place auction statistics.
   *
   * @param int $auction_id
   *   The Auction ID.
   *
   * @command auctioneer:place_auction_statistics
   *
   * @usage drush auctioneer:place_auction_statistics xxx
   *   Place auction statistics to the specified auction ID.
   *
   * @aliases auctioneer-pas
   */
  public function placeAuctionStatistics(int $auction_id) {
    $entity_type_manager = $this->auctionTypeManager->entityTypeManager();
    // Needed to load auction.
    $auction = $entity_type_manager->getStorage('auction')->load($auction_id);
    if ($auction) {
      $this->auctionTypeManager->setAuctionStatistics($auction);
      $this->output()->writeln(dt('Statistics for auction "@auction_label" were placed successfully.', [
        '@auction_label' => $auction->label(),
      ]));
    }
    else {
      $this->output()->writeln(dt('No auction with id "@auction_id" was found.', [
        '@auction_id' => $auction_id,
      ]));
    }
  }

  /**
   * Display auction statistics.
   *
   * @param int $auction_id
   *   The Auction ID.
   * @param array $options
   *   Options for this command.
   *
   * @command auctioneer:display_auction_statistics
   *
   * @option format
   *   The format to display statistics. Use "json" to get a plain JSON object.
   *
   * @usage drush auctioneer:display_auction_statistics xxx
   *   Get statistics. Basic usage.
   * @usage drush auctioneer:display_auction_statistics xxx --format=json
   *   Get statistics. Use JSON format.
   *
   * @aliases auctioneer-das
   */
  public function displayAuctionStatistics(
    int $auction_id,
    array $options = [
      'format' => NULL,
    ]) {
    $stats = [];
    $entity_type_manager = $this->auctionTypeManager->entityTypeManager();
    // Needed to load auction.
    $auction = $entity_type_manager->getStorage('auction')->load($auction_id);
    if ($auction) {
      $stats = $this->auctionTypeManager->getAuctionStatistics($auction);
    }
    // Now, displaying in expected format.
    if ($options['format'] == 'json') {
      $this->output()->writeln(json_encode($stats));
    }
    else {
      $this->output()->writeln(var_export($stats));
    }
  }

}
