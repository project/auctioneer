<?php

namespace Drupal\auctioneer;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of auctioneer handlers.
 */
class HandlerPluginsCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

}
