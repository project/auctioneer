<?php

namespace Drupal\auctioneer;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer\Entity\BidTypeInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides managing service for auction types.
 */
class AuctionTypeManager implements AuctionTypeManagerInterface {

  /**
   * The auction type plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $auctionTypePluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The auction field manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $auctionFieldManager;

  /**
   * The database handler.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current account interface.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor method.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $auction_type_plugin_manager
   *   The auction type plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\auctioneer\AuctionFieldManagerInterface $auction_field_manager
   *   Auction field manager for easier entity field management.
   * @param \Drupal\Core\Database\Connection $database
   *   The database handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    PluginManagerInterface $auction_type_plugin_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AuctionFieldManagerInterface $auction_field_manager,
    Connection $database,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory,
    AccountProxyInterface $account,
    ModuleHandlerInterface $module_handler
  ) {
    $this->auctionTypePluginManager = $auction_type_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->auctionFieldManager = $auction_field_manager;
    $this->database = $database;
    $this->loggerFactory = $logger_factory;
    $this->configFactory = $config_factory;
    $this->account = $account;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function auctionPluginManager() : PluginManagerInterface {
    return $this->auctionTypePluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function logger(string $channel) : LoggerInterface {
    return $this->loggerFactory->get($channel);
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeManager() : EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function auctionFieldManager() : AuctionFieldManagerInterface {
    return $this->auctionFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public function database() : Connection {
    return $this->database;
  }

  /**
   * {@inheritdoc}
   */
  public function configFactory() : ConfigFactoryInterface {
    return $this->configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableAuctionTypeNames($machine_name = FALSE) : array {
    $options = [];
    foreach ($this->auctionTypePluginManager->getDefinitions() as $plugin_definition) {
      $options[$plugin_definition['id']] = $machine_name ? $plugin_definition['id'] : $plugin_definition['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableBidTypeNames($machine_name = FALSE) : array {
    $options = [];
    $auction_type_names = $this->getAvailableAuctionTypeNames($machine_name);
    if (!empty($auction_type_names)) {
      $bid_types = $this->entityTypeManager->getStorage('bid_type')->loadMultiple();
      foreach ($bid_types as $bid_type) {
        $group_key = $machine_name ? $bid_type->getPluginName() : $auction_type_names[$bid_type->getPluginName()];
        // Let's group on a common set of items per auction type.
        $options[$group_key][$bid_type->id()] = $machine_name ? $bid_type->id() : $bid_type->label();
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeAuctionTypeBaseConfigurations(string $bid_type_id, AuctionTypeInterface $auction_type, bool $lock_fields = TRUE) : AuctionTypeInterface {
    $entity_type_id = 'auction';
    $bundle = $auction_type->id();
    $bid_type = $this->entityTypeManager->getStorage('bid_type')->load($bid_type_id);
    if ($bid_type instanceof BidTypeInterface) {
      $auction_plugin_id = $bid_type->getPluginName();
      if (!$auction_plugin_id) {
        return $auction_type;
      }
      $plugin_instance = $this->auctionTypePluginManager->createInstance($auction_plugin_id);
      if ($plugin_instance) {
        $fields = $plugin_instance->getAuctionBaseFieldDefinitions();
        $this->auctionFieldManager->provideEntityTypeFields($fields, $entity_type_id, $bundle, $lock_fields);
        $auction_type->setBidTypeName($bid_type_id);
        $auction_type->save();
      }
    }

    return $auction_type;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeBidTypeBaseConfigurations($auction_plugin_id, BidTypeInterface $bid_type, $lock_fields = TRUE) : BidTypeInterface {
    $plugin_instance = $this->auctionTypePluginManager->createInstance($auction_plugin_id);
    if ($plugin_instance) {
      $fields = $plugin_instance->getBidBaseFieldDefinitions();
      $entity_type_id = 'bid';
      $bundle = $bid_type->id();
      $this->auctionFieldManager->provideEntityTypeFields($fields, $entity_type_id, $bundle, $lock_fields);
      $bid_type->setPluginName($auction_plugin_id);
      $bid_type->save();
    }

    return $bid_type;
  }

  /**
   * {@inheritdoc}
   */
  public static function getAllowedAuctionStates() : array {
    $states = [
      'opened' => t('Opened'),
      'paused' => t('Paused'),
      'closed' => t('Closed'),
    ];

    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function processBidForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    // Plugin instance and validation.
    $auction_plugin_id = $bid->getBidType()->getPluginName();
    $plugin_instance = $this->auctionTypePluginManager->createInstance($auction_plugin_id);
    if ($plugin_instance) {
      $plugin_instance->processBidForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateBidForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    // Plugin instance and validation.
    $auction_plugin_id = $bid->getBidType()->getPluginName();
    $plugin_instance = $this->auctionTypePluginManager->createInstance($auction_plugin_id);
    if ($plugin_instance) {
      $plugin_instance->validateBidForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitBidForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\auctioneer\Entity\BidInterface $bid */
    $bid = $form_state->getFormObject()->getEntity();
    // Plugin instance and validation.
    $auction_plugin_id = $bid->getBidType()->getPluginName();
    $plugin_instance = $this->auctionTypePluginManager->createInstance($auction_plugin_id);
    if ($plugin_instance) {
      $plugin_instance->submitBidForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAuctionStatistics(AuctionInterface $auction) {
    /** @var \Drupal\auctioneer\Entity\BidTypeInterface $bid_type */
    $bid_type = $auction->getAuctionType()->getBidType();
    $plugin_definition = $this->getBidTypePluginDefinition($bid_type);
    $total_bids = $this->getAuctionBidsCount($auction);
    $initial_bid = $auction->getFirstBid();
    $last_bid = $auction->getLastBid();
    $context = [];
    // Allow other modules to alter/add contexts.
    $this->moduleHandler->invokeAll(
      'auctioneer_auction_context_alter',
      [
        $auction,
        $plugin_definition,
        &$context,
      ]
    );
    // Now, let's insert/update our modified record.
    $this->database->merge('auctioneer_auction_statistics')
      ->key('auction_id', $auction->id())
      ->fields([
        'total_amount_bids' => $total_bids,
        'timestamp' => time(),
        'initial_bid' => $initial_bid ? $initial_bid->id() : NULL,
        'last_bid' => $last_bid ? $last_bid->id() : NULL,
        'context' => implode(',', array_values($context)),
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getAuctionStatistics(AuctionInterface $auction) : array {
    $result = $this->database->select('auctioneer_auction_statistics', 'ac')
      ->fields('ac')
      ->condition('auction_id', $auction->id())
      ->execute()
      ->fetchAssoc();

    return is_array($result) ? $result : [];
  }

  /**
   * {@inheritdoc}
   */
  public function purgeAuctionStatistics(AuctionInterface $auction) {
    $this->database->delete('auctioneer_auction_statistics')
      ->condition('auction_id', $auction->id())
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function setAuctionHammerBid(BidInterface $bid, bool $close_auction = TRUE) {
    $auction = $bid->getAuction();
    if ($auction instanceof AuctionInterface) {
      $this->database->merge('auctioneer_auction_hammer')
        ->key('bid', $bid->id())
        ->fields([
          'auction' => $auction->id(),
          'uid' => $this->account->id(),
          'timestamp' => time(),
        ])
        ->execute();
      if ($auction->getAuctionState() != 'closed' && $close_auction) {
        $auction->set('auction_state', 'closed');
        $auction->save();
      }
      // Re-calculate auction statistics.
      $this->setAuctionStatistics($auction);
      // Let's execute the handlers associated to this action.
      $bid->performHandlersExecution('hammer');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAuctionHammerBidData(BidInterface $bid) : array {
    $result = $this->database->select('auctioneer_auction_hammer', 'h')
      ->fields('h')
      ->condition('bid', $bid->id())
      ->execute()
      ->fetchAssoc();

    return is_array($result) ? $result : [];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAuctionHammerBidData(BidInterface $bid) {
    $this->database->delete('auctioneer_auction_hammer')
      ->condition('bid', $bid->id())
      ->execute();
    $auction = $bid->getAuction();
    if ($auction) {
      // Let's reset auction statistics.
      $this->setAuctionStatistics($auction);
    }
    // Let's execute the handlers associated to this action.
    $bid->performHandlersExecution('unhammer');
  }

  /**
   * {@inheritdoc}
   */
  public function purgeAuctionHammerBid(AuctionInterface $auction) {
    $hammer = $this->getAuctionHammerBid($auction);
    if ($hammer instanceof BidInterface) {
      $this->database->delete('auctioneer_auction_hammer')
        ->condition('bid', $hammer->id())
        ->execute();
      // Let's reset auction statistics.
      $this->setAuctionStatistics($auction);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function purgeOrphanBids(array $configuration = []) {
    $stats = [
      'count' => [
        '_total' => 0,
      ],
    ];
    // Some needed variables to make our query more flexible.
    $limit = $configuration['limit'] ?? 30;
    // Our query itself.
    $query = $this->database->select('bid_field_data', 'bfd');
    $query->fields('bfd', ['id']);
    $query->leftJoin('auction_field_data', 'afd', 'bfd.auction_id = afd.id');
    $query->isNull('afd.id');
    $query->orderBy('bfd.id', 'ASC');
    $query->range(0, $limit);
    $result = $query->execute()->fetchCol();
    // Now, let's delete bids.
    $bid_storage = $this->entityTypeManager->getStorage('bid');
    $orphan_bids = !empty($result) ? $bid_storage->loadMultiple($result) : [];
    if (!empty($orphan_bids)) {
      $stats['count']['_total'] = count($orphan_bids);
      $bid_storage->delete($orphan_bids);
    }

    return $stats;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuctionHammerBid(AuctionInterface $auction) {
    // We need to query for bids into the related tables.
    $query = $this->database->select('bid_field_data', 'bfd');
    $query->fields('bfd', ['id']);
    $query->innerJoin('auctioneer_auction_hammer', 'h', 'h.bid = bfd.id');
    $query->condition('bfd.auction_id', $auction->id());
    $query->range(0, 1);
    $result = $query->execute()->fetchCol();

    return !empty($result) ? $this->entityTypeManager->getStorage('bid')->load(reset($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getBidTypePluginDefinition(BidTypeInterface $bid_type) : array {
    $plugin_definition = $this->auctionTypePluginManager->getDefinition($bid_type->getPluginName());

    return is_array($plugin_definition) ? $plugin_definition : [];
  }

  /**
   * Helper function to get a number of placed bids per auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   Thye auction to get counted.
   */
  public function getAuctionBidsCount(AuctionInterface $auction) {
    $total = $this->database->select('bid_field_data', 'bfd')
      ->condition('auction_id', $auction->id())
      ->countQuery()
      ->execute()
      ->fetchField();

    return $total;
  }

}
