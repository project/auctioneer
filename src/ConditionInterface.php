<?php

namespace Drupal\auctioneer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Defines the interface for condition plugins.
 */
interface ConditionInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Calculate dependencies.
   *
   * @return array
   *   The condition calculated dependencies (not implemented yet).
   */
  public function calculateDependencies();

  /**
   * Provide a default configuration.
   *
   * @return array
   *   The condition default configuration.
   */
  public function defaultConfiguration();

  /**
   * Set condition configuration.
   *
   * @param array $configuration
   *   The condition array settings.
   *
   * @return \Drupal\auctioneer\HandlerInterface
   *   The condition instance.
   */
  public function setConfiguration(array $configuration);

  /**
   * Get condition configuration.
   *
   * @return array
   *   The condition configuration.
   */
  public function getConfiguration();

  /**
   * Constructs the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param array $context
   *   The needed values for this operation.
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []);

  /**
   * Validate the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param array $context
   *   The needed values for this operation.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []);

  /**
   * Submit the configuration form.
   *
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The $form_state object.
   * @param array $context
   *   The needed values for this operation.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, array $context = []);

  /**
   * Set label for this condition instance.
   *
   * @param string $label
   *   The label to be used.
   *
   * @return \Drupal\auctioneer\ConditionInterface
   *   The condition instance.
   */
  public function setLabel(string $label);

  /**
   * Get specified label on this condition.
   *
   * @return string
   *   The condition label.
   */
  public function getLabel();

  /**
   * Set condition description.
   *
   * @param string $description
   *   The condition description.
   *
   * @return \Drupal\auctioneer\ConditionInterface
   *   The condition instance.
   */
  public function setDescription(string $description);

  /**
   * Get condition description.
   *
   * @return string
   *   The condition description.
   */
  public function getDescription();

  /**
   * Set condition weight.
   *
   * @param int $weight
   *   The weight for this condition.
   *
   * @return \Drupal\auctioneer\ConditionInterface
   *   The condition instance.
   */
  public function setWeight(int $weight);

  /**
   * Get condition weight.
   *
   * @return int
   *   The condition weight.
   */
  public function getWeight();

  /**
   * Evaluates the condition.
   *
   * @param array $params
   *   The key/value needed parameters depending on condition scope. If
   *   condition scope is one of "bid, auction", then a content entity
   *   will be available at $params['entity']. This is, because we want
   *   to allow conditions to be "shareable" in multiple purposes; not only
   *   for handlers or content entities.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate(array $params = []) : bool;

}
