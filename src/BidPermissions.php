<?php

namespace Drupal\auctioneer;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for bids.
 */
class BidPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The auction type manager.
   *
   * @var \Drupal\auctioneer\AuctionTypeManagerInterface
   */
  protected $auctionTypeManager;

  /**
   * Constructs the "HammerBidForm" form.
   *
   * @param \Drupal\auctioneer\AuctionTypeManagerInterface $auction_type_manager
   *   The auction type manager.
   */
  public function __construct(AuctionTypeManagerInterface $auction_type_manager) {
    $this->auctionTypeManager = $auction_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('auctioneer.auction_type.manager')
    );
  }

  /**
   * Get specific permissions for bid types.
   */
  public function hammerBidTypePermissions() {
    $permissions = [];
    $bid_types = $this->auctionTypeManager->getAvailableBidTypeNames();
    if (!empty($bid_types)) {
      foreach ($bid_types as $group => $types) {
        foreach ($types as $type_id => $type_label) {
          $permissions += [
            "auctioneer hammer bid type $type_id any auction" => [
              'title' => $this->t('%group_label: Hammer bid type %type_label in any auction', [
                '%group_label' => $group,
                '%type_label' => $type_label,
              ]),
            ],
            "auctioneer hammer bid type $type_id own auction" => [
              'title' => $this->t('%group_label: Hammer bid type %type_label in own auction', [
                '%group_label' => $group,
                '%type_label' => $type_label,
              ]),
            ],
          ];
        }
      }
    }

    return $permissions;
  }

}
