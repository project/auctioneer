<?php

namespace Drupal\auctioneer\Event;

use Drupal\auctioneer\Entity\BidInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the hammer event.
 */
class HammerBidEvent extends Event {

  const EVENT_NAME = 'auctioneer.bid.event.hammer';

  /**
   * The bid entity to be processed.
   *
   * @var \Drupal\auctioneer\Entity\BidInterface
   */
  protected $bid;

  /**
   * The event action.
   *
   * @var string
   */
  protected $action;

  /**
   * Constructor method.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid object.
   * @param string $action
   *   String telling if hammer/unhammer.
   */
  public function __construct(BidInterface $bid, string $action) {
    $this->bid = $bid;
    $this->action = $action;
  }

  /**
   * Get bid object.
   */
  public function bid() {
    return $this->bid;
  }

  /**
   * Get bid object.
   */
  public function action() {
    return $this->action;
  }

}
