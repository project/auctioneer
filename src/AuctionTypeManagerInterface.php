<?php

namespace Drupal\auctioneer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\auctioneer\Entity\AuctionInterface;
use Drupal\auctioneer\Entity\AuctionTypeInterface;
use Drupal\auctioneer\Entity\BidInterface;
use Drupal\auctioneer\Entity\BidTypeInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides managing service for auction.
 */
interface AuctionTypeManagerInterface {

  /**
   * Get auction plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The auction plugin manager.
   */
  public function auctionPluginManager() : PluginManagerInterface;

  /**
   * Get entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function entityTypeManager() : EntityTypeManagerInterface;

  /**
   * Get auction field manager.
   *
   * @return \Drupal\auctioneer\AuctionFieldManagerInterface
   *   The auction field manager.
   */
  public function auctionFieldManager() : AuctionFieldManagerInterface;

  /**
   * Return the database coneection manager.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  public function database() : Connection;

  /**
   * Get logger channel.
   *
   * @param string $channel
   *   The channel key to save logs on.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger interface.
   */
  public function logger(string $channel) : LoggerInterface;

  /**
   * The config factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The configuration factory instance.
   */
  public function configFactory() : ConfigFactoryInterface;

  /**
   * Get available auction type names.
   *
   * @param bool $machine_name
   *   Boolean indicating if only machine names should be provided.
   *
   * @return array
   *   The auction type names.
   */
  public function getAvailableAuctionTypeNames($machine_name = FALSE) : array;

  /**
   * Get available bid type names.
   *
   * @param bool $machine_name
   *   Boolean indicating if only machine names should be provided.
   *
   * @return array
   *   The bid type names.
   */
  public function getAvailableBidTypeNames($machine_name = FALSE) : array;

  /**
   * Initialize needed configurations for an auction entity type.
   *
   * @param string $bid_type_id
   *   The bid machine name/id.
   * @param \Drupal\auctioneer\Entity\AuctionTypeInterface $auction_type
   *   The auction type configuration entity.
   * @param bool $lock_fields
   *   Boolean indicating if fields should be locked.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface
   *   The changed/unchanged entity interface.
   */
  public function initializeAuctionTypeBaseConfigurations(string $bid_type_id, AuctionTypeInterface $auction_type, bool $lock_fields = TRUE) : AuctionTypeInterface;

  /**
   * Initialize needed fields/configurations for a bid type.
   *
   * @param string $auction_plugin_id
   *   The auction plugin ID.
   * @param \Drupal\auctioneer\Entity\BidTypeInterface $bid_type
   *   The bid type object.
   * @param bool $lock_fields
   *   Tells if fields should be blocked.
   *
   * @return \Drupal\auctioneer\Entity\BidTypeInterface
   *   The updated bid type.
   */
  public function initializeBidTypeBaseConfigurations($auction_plugin_id, BidTypeInterface $bid_type, $lock_fields = TRUE) : BidTypeInterface;

  /**
   * Get allowed auction states.
   *
   * @return array
   *   The key/value pairs of valid auction states.
   */
  public static function getAllowedAuctionStates() : array;

  /**
   * Allow plugins to deal with bid form building.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function processBidForm(array &$form, FormStateInterface $form_state);

  /**
   * Allow plugins to deal with bid form validation.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function validateBidForm(array &$form, FormStateInterface $form_state);

  /**
   * Allow plugins to deal with bid form submission.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state element.
   */
  public function submitBidForm(array &$form, FormStateInterface $form_state);

  /**
   * Set auction statistics for a given auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction to react on.
   */
  public function setAuctionStatistics(AuctionInterface $auction);

  /**
   * Get auction statistics for a given auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction to react on.
   */
  public function getAuctionStatistics(AuctionInterface $auction) : array;

  /**
   * Purge auction statistics.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction to purge statistics on.
   */
  public function purgeAuctionStatistics(AuctionInterface $auction);

  /**
   * Set bid as winner.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid to be marked as winner.
   * @param bool $close_auction
   *   Boolean telling if auction should be closed or not.
   */
  public function setAuctionHammerBid(BidInterface $bid, bool $close_auction = TRUE);

  /**
   * Check if a given bid is a hammer and fetch data.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid to be checked.
   */
  public function getAuctionHammerBidData(BidInterface $bid) : array;

  /**
   * Remove a specific bid from the hammer list.
   *
   * @param \Drupal\auctioneer\Entity\BidInterface $bid
   *   The bid to be removed.
   */
  public function deleteAuctionHammerBidData(BidInterface $bid);

  /**
   * Purge all hammer bids from a specified auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction to purge hammer bids on.
   */
  public function purgeAuctionHammerBid(AuctionInterface $auction);

  /**
   * Delete bids where its referenced auction is no longer available.
   *
   * @param array $configuration
   *   Extra parameters passed to this operation.
   */
  public function purgeOrphanBids(array $configuration = []);

  /**
   * Get all hammer bids from a specified auction.
   *
   * @param \Drupal\auctioneer\Entity\AuctionInterface $auction
   *   The auction to get hammer bids from.
   */
  public function getAuctionHammerBid(AuctionInterface $auction);

  /**
   * Get plugin definition for a given bid type.
   *
   * @param \Drupal\auctioneer\Entity\BidTypeInterface $bid_type
   *   The bid type object.
   */
  public function getBidTypePluginDefinition(BidTypeInterface $bid_type) : array;

}
