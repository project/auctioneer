<?php

namespace Drupal\auctioneer;

use Drupal\Core\Entity\Display\EntityDisplayInterface;

/**
 * Provides managing service for auction.
 */
interface AuctionFieldManagerInterface {

  /**
   * Set entity bundle required fields.
   *
   * @param array $field_definitions
   *   The fields to attach to this entity bundle.
   * @param string $entity_type_id
   *   The entity type ID for this operation.
   * @param string $bundle
   *   The bundle to add fields on.
   * @param bool $lock_fields
   *   Boolean telling if fields should be blocked or not.
   */
  public function provideEntityTypeFields(array $field_definitions, string $entity_type_id, string $bundle, bool $lock_fields = TRUE);

  /**
   * Get an entity display for the given entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $display_context
   *   The display context ('view' or 'form').
   *
   * @throws \InvalidArgumentException
   *   Thrown when an invalid display context is provided.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface
   *   The entity display.
   */
  public function getEntityDisplay($entity_type, $bundle, $display_context) : EntityDisplayInterface;

}
