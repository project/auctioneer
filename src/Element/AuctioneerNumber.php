<?php

namespace Drupal\auctioneer\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides an "auctioneer_number" form element.
 *
 * @FormElement("auctioneer_number")
 */
class AuctioneerNumber extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#size' => 30,
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderNumber'],
        [$class, 'preRenderGroup'],
      ],
      '#input' => TRUE,
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Builds the auctioneer_number form element.
   *
   * @param array $element
   *   The initial auctioneer_number form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built auctioneer_number form element.
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = $element['#default_value'] ?? [];
    $element['#tree'] = TRUE;
    $element['number'] = [
      '#type' => 'textfield',
      '#title' => $element['#title'],
      '#default_value' => $default_value['number'] ?? NULL,
      '#size' => $element['#size'] ?? 30,
      '#required' => $element['#required'],
    ];
    if (isset($element['#ajax'])) {
      $element['number']['#ajax'] = $element['#ajax'];
    }
    unset($element['#size']);
    unset($element['#maxlength']);
    unset($element['#ajax']);

    return $element;
  }

  /**
   * Prepares a #type 'auctioneer_number' render element for input.html.twig.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for input.html.twig.
   */
  public static function preRenderNumber(array $element) {
    Element::setAttributes(
      $element,
      [
        'id',
        'name',
        'value',
        'size',
        'maxlength',
        'placeholder',
      ]
    );
    static::setAttributes($element, ['form-text']);

    return $element;
  }

}
