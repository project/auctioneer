<?php

namespace Drupal\auctioneer;

use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Provides managing service for field operations.
 */
class AuctionFieldManager implements AuctionFieldManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function provideEntityTypeFields(array $field_definitions, string $entity_type_id, string $bundle, bool $lock_fields = TRUE) {
    foreach ($field_definitions as $field_name => $field_definition) {
      $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
      if (empty($field_storage)) {
        $field_storage = FieldStorageConfig::create([
          'field_name' => $field_name,
          'entity_type' => $entity_type_id,
          'type' => $field_definition->getType(),
          'cardinality' => $field_definition->getCardinality(),
          'settings' => $field_definition->getSettings(),
          'translatable' => $field_definition->isTranslatable(),
          'locked' => $lock_fields,
        ]);
        $field_storage->save();
      }
      $field_config = FieldConfig::loadByName($entity_type_id, $bundle, $field_name);
      if (empty($field_config)) {
        $field_config = FieldConfig::create([
          'field_storage' => $field_storage,
          'bundle' => $bundle,
          'label' => $field_definition->getLabel(),
          'required' => $field_definition->isRequired(),
          'settings' => $field_definition->getSettings(),
          'translatable' => $field_definition->isTranslatable(),
          'default_value' => $field_definition->getDefaultValueLiteral(),
          'default_value_callback' => $field_definition->getDefaultValueCallback(),
        ]);
        $field_config->save();
      }
      // Show the field on default entity displays, if specified.
      if ($form_display_options = $field_definition->getDisplayOptions('form')) {
        $form_display = $this->getEntityDisplay($entity_type_id, $bundle, 'form');
        if (!$form_display->getComponent($field_name)) {
          $form_display->setComponent($field_name, $form_display_options);
          $form_display->save();
        }
      }
      if ($view_display_options = $field_definition->getDisplayOptions('view')) {
        $view_display = $this->getEntityDisplay($entity_type_id, $bundle, 'view');
        if (!$view_display->getComponent($field_name)) {
          $view_display->setComponent($field_name, $view_display_options);
          $view_display->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityDisplay($entity_type, $bundle, $display_context) : EntityDisplayInterface {
    if (!in_array($display_context, ['view', 'form'])) {
      throw new \InvalidArgumentException(sprintf('Invalid display_context %s passed. It must be "view" or "form".', $display_context));
    }
    $storage = $this->entityTypeManager->getStorage('entity_' . $display_context . '_display');
    $display = $storage->load($entity_type . '.' . $bundle . '.default');
    if (!$display) {
      $display = $storage->create([
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }

    return $display;
  }

}
