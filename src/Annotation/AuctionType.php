<?php

namespace Drupal\auctioneer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the auction type plugin annotation object.
 *
 * Plugin namespace: Plugin\auctioneer\AuctionType.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class AuctionType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The auction type label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
