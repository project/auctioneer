<?php

namespace Drupal\auctioneer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the handler plugin annotation object.
 *
 * Plugin namespace: Plugin\auctioneer\AuctionHandler.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Handler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The auction handler label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The auction handler description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The auction handler scopes.
   *
   * @var string
   *
   * Valid options: bid, auction (or both, separated by comma).
   */
  public $scope;

}
