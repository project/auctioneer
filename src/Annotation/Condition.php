<?php

namespace Drupal\auctioneer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the condition plugin annotation object.
 *
 * Plugin namespace: Plugin\auctioneer\Condition.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Condition extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The condition label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The condition description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The plugin scope.
   *
   * @var string
   */
  public $scope;

}
